#include "tutorial_audio.h"

// GUItool: begin automatically generated code
AudioSynthWaveformModulated tutorialAudio1Osc;   //xy=214.99999999999997,349.99999999999994
AudioFilterStateVariable tutorialAudio1Filter;        //xy=474.9999885559082,338.33334732055664
AudioMixer4              tutorialAudio1Mixer;         //xy=688.3333215713501,341.6666793823242
AudioOutputAnalogStereo  tutorialAudio1Dacs;          //xy=943.3333333333331,341.66666666666663
AudioConnection          tutorialAudio1Patch1(tutorialAudio1Osc, 0, tutorialAudio1Filter, 0);
AudioConnection          tutorialAudio1Patch2(tutorialAudio1Filter, 0, tutorialAudio1Mixer, 0);
AudioConnection          tutorialAudio1Patch3(tutorialAudio1Filter, 1, tutorialAudio1Mixer, 1);
AudioConnection          tutorialAudio1Patch4(tutorialAudio1Filter, 2, tutorialAudio1Mixer, 2);
AudioConnection          tutorialAudio1Patch5(tutorialAudio1Mixer, 0, tutorialAudio1Dacs, 0);
AudioConnection          tutorialAudio1Patch6(tutorialAudio1Mixer, 0, tutorialAudio1Dacs, 1);
// GUItool: end automatically generated code

TutorialAudio::TutorialAudio() : Program() {
  Setup();
}

void TutorialAudio::Setup() {
  // initial audio setup
  tutorialAudio1Osc.begin(1, 440, WAVEFORM_SQUARE);
  tutorialAudio1Filter.resonance(0.7);
  tutorialAudio1Filter.frequency(1000);
  tutorialAudio1Mixer.gain(0,0.0);
  tutorialAudio1Mixer.gain(1,0.0);
  tutorialAudio1Mixer.gain(2,0.0);

  a = Engine::Instance()->pots[0]->ReadAs128();
  b = Engine::Instance()->pots[1]->ReadAs128();
  c = Engine::Instance()->pots[2]->ReadAs128();
  waveform = WAVEFORM_SQUARE;
}

void TutorialAudio::Run() {
}

void TutorialAudio::Loop() {
  Engine::Instance()->modeLeds[0]->Set(5);
  Engine::Instance()->oled->SetScale(2);

  // get value from pots
  int potValue0 = Engine::Instance()->pots[0]->ReadAs128();
  int potValue1 = Engine::Instance()->pots[1]->ReadAs128();
  int potValue2 = Engine::Instance()->pots[2]->ReadAs128();

  // check if param is changed
  if (potValue0 != a) {
    // remap the values to the desired ranges
    int pitch = map(potValue0, 0, 127, 80, 440);
    // set the params to their objects
    tutorialAudio1Osc.frequency(pitch);
  }

  // check if param is changed
  if (potValue1 != b) {
    // remap the values to the desired ranges
    float cutoff = map(potValue1, 0, 127, 400, 5000);
    // set the params to their objects
    tutorialAudio1Filter.frequency(cutoff);
  }

  // check if param is changed
  if (potValue2 != c) {
    // remap the values to the desired ranges
    float vol = potValue2 / 127.0;
    // set the params to their objects
    tutorialAudio1Mixer.gain(0,vol);
  }

  // display the params
  Engine::Instance()->oled->DrawParamLine(0, "PITCH", potValue0);
  Engine::Instance()->oled->DrawParamLine(1, "CUTOFF", potValue1);
  Engine::Instance()->oled->DrawParamLine(2, "VOL", potValue2);

  if (waveform == WAVEFORM_SQUARE) {
    Engine::Instance()->buttonLeds[0]->Set(5);
  } else if (waveform == WAVEFORM_SINE) {
    Engine::Instance()->buttonLeds[1]->Set(5);
  } else if (waveform == WAVEFORM_SAWTOOTH) {
    Engine::Instance()->buttonLeds[2]->Set(5);
  }
}

void TutorialAudio::OnKeyDown(int key) {
  // change the waveform when a button is pressed
  if (key == 0) {
    waveform = WAVEFORM_SQUARE;
    tutorialAudio1Osc.begin(waveform);
  } else if (key == 1) {
    waveform = WAVEFORM_SINE;
    tutorialAudio1Osc.begin(waveform);
    tutorialAudio1Osc.begin(WAVEFORM_SINE);
  } else if (key == 2) {
    waveform = WAVEFORM_SAWTOOTH;
    tutorialAudio1Osc.begin(waveform);
  } else if (key == 4) {
    Engine::Instance()->programSwitcher->NextProgram();
  }
}
