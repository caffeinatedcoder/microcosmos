#include "tutorial_faust.h"

/*
FaustVoice voice1;
AudioOutputAnalogStereo  microcosmosTutorialFaustDacs;
AudioConnection          mtfConnection1(voice1, 0, microcosmosTutorialFaustDacs, 0);
AudioConnection          mtfConnection2(voice1, 0, microcosmosTutorialFaustDacs, 1);
*/

TutorialFaust::TutorialFaust() : Program() {
  Setup();
}

void TutorialFaust::Setup() {
}

void TutorialFaust::Run() {
}

void TutorialFaust::Loop() {
  /*
  Engine::Instance()->modeLeds[0]->Set(5);

  Engine::Instance()->oled->SetScale(2);
  Engine::Instance()->oled->DrawStringLine(0, "AUDIO WITH");
  Engine::Instance()->oled->DrawStringLine(1, "FAUST");

  // get value from pots
  int potValue0 = Engine::Instance()->pots[0]->ReadAs64();
  int potValue1 = Engine::Instance()->pots[1]->ReadAs64();

  // remap the values to the desired ranges
  int pitch = map(potValue0, 0, 64, 80, 440);
  float cutoff = map(potValue1, 0, 64, 400, 5000);

  // set the params to their objects
  voice1.setParamValue("pitch", pitch);
  voice1.setParamValue("cutoff", cutoff);

  voice1.setParamValue("trigger", false);
  */
}

void TutorialFaust::OnKeyDown(int key) {
  /*
  if (key == 0) {
    voice1.setParamValue("trigger", true);
    Engine::Instance()->buttonLeds[0]->Set(5);
  } else if (key == 4) {
    Engine::Instance()->programSwitcher->NextProgram();
  }
  */
}
