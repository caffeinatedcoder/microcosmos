#include "engine.h"

Engine* Engine::_instance = NULL;

Engine* Engine::Instance() {
	if(_instance == NULL){
		_instance = new Engine();
	}
	return _instance;
}

Engine::~Engine() {
	if(_instance != 0) delete _instance;
}

Engine::Engine() {
	_mainTimer = new Timer();
	programSwitcher = new ProgramSwitcher();

	for (int i=0; i<BUTTONS; i++) {
		buttons[i] = new Button();
	}

	for (int i=0; i<POTS; i++) {
		pots[i] = new Pot();
	}

	for (int i=0; i<ENCODERS; i++) {
		encoders[i] = new Encoder();
		encoderButtons[i] = new Button();
	}

	oled = new Oled();

	for (int i=0; i<BUTTONS_LEDS; i++) {
		buttonLeds[i] = new Led();
	}

	for (int i=0; i<MODE_LEDS; i++) {
		modeLeds[i] = new Led();
	}

	for (int i=0; i<IO_LEDS; i++) {
		ioLeds[i] = new Led();
	}
}

void Engine::Setup() {
	_mainTimer->Setup();

	buttons[0]->Setup(BUTTON_A_PIN, buttonAInterruptCallback);
	buttons[1]->Setup(BUTTON_B_PIN, buttonBInterruptCallback);
	buttons[2]->Setup(BUTTON_C_PIN, buttonCInterruptCallback);
	buttons[3]->Setup(BUTTON_D_PIN, buttonDInterruptCallback);

	pots[0]->Setup(ANALOG_POT_PIN_A, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);
	pots[1]->Setup(ANALOG_POT_PIN_B, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);
	pots[2]->Setup(ANALOG_POT_PIN_C, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);

	encoders[0]->Setup(ENCODER_A_B_PIN, ENCODER_A_A_PIN, encoderAInterruptCallback);
	encoders[1]->Setup(ENCODER_B_B_PIN, ENCODER_B_A_PIN, encoderBInterruptCallback);
	encoders[2]->Setup(ENCODER_C_B_PIN, ENCODER_C_A_PIN, encoderCInterruptCallback);

	encoderButtons[0]->Setup(ENCODER_A_BUTTON_PIN, encoderButtonAInterruptCallback);
	encoderButtons[1]->Setup(ENCODER_B_BUTTON_PIN, encoderButtonBInterruptCallback);
	encoderButtons[2]->Setup(ENCODER_C_BUTTON_PIN, encoderButtonCInterruptCallback);

	oled->Setup();

	buttonLeds[0]->Setup(LED_BTN_A_PIN, false);
	buttonLeds[1]->Setup(LED_BTN_B_PIN, false);
	buttonLeds[2]->Setup(LED_BTN_C_PIN, false);
	buttonLeds[3]->Setup(LED_BTN_D_PIN, false);

	modeLeds[0]->Setup(LED_MODE_A_PIN, false);
	modeLeds[1]->Setup(LED_MODE_B_PIN, false);
	modeLeds[2]->Setup(LED_MODE_C_PIN, false);
	modeLeds[3]->Setup(LED_MODE_D_PIN, false);

	ioLeds[0]->Setup(LED_IO_A_PIN, false);
	ioLeds[1]->Setup(LED_IO_B_PIN, false);
	ioLeds[2]->Setup(LED_IO_C_PIN, false);
	ioLeds[3]->Setup(LED_IO_D_PIN, false);

	Serial.println("engine setup ok");
}

void Engine::Loop() {
	// execute program not constrained to FPS
	if (programSwitcher->GetCurrentProgram() != NULL) {
		programSwitcher->GetCurrentProgram()->Run();
	}

  unsigned long elapsed = _mainTimer->GetElapsed();

  if (elapsed >= FPS) {
    _mainTimer->Update();

		oled->ClearDisplay();

		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Reset();
		}

		for (int i=0; i<MODE_LEDS; i++) {
			modeLeds[i]->Reset();
		}

		for (int i=0; i<IO_LEDS; i++) {
			ioLeds[i]->Reset();
		}

		// execute program
		if (programSwitcher->GetCurrentProgram() != NULL) {
			programSwitcher->GetCurrentProgram()->Loop();
		}

		oled->Render();

		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Show();
		}

		for (int i=0; i<MODE_LEDS; i++) {
			modeLeds[i]->Show();
		}

		for (int i=0; i<IO_LEDS; i++) {
			ioLeds[i]->Show();
		}
  }
}

void Engine::encoderAInterruptCallback() {
	Engine::Instance()->encoders[0]->InterruptCallback();
}

void Engine::encoderBInterruptCallback() {
	Engine::Instance()->encoders[1]->InterruptCallback();
}

void Engine::encoderCInterruptCallback() {
	Engine::Instance()->encoders[2]->InterruptCallback();
}

void Engine::buttonAInterruptCallback() {
	int btn = 0;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonBInterruptCallback() {
	int btn = 1;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonCInterruptCallback() {
	int btn = 2;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonDInterruptCallback() {
	int btn = 3;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::encoderButtonAInterruptCallback() {
	int btn = 0;
	if (Engine::Instance()->encoderButtons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn + 4);
	}
}

void Engine::encoderButtonBInterruptCallback() {
	int btn = 1;
	if (Engine::Instance()->encoderButtons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn + 4);
	}
}

void Engine::encoderButtonCInterruptCallback() {
	int btn = 2;
	if (Engine::Instance()->encoderButtons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn + 4);
	}
}
