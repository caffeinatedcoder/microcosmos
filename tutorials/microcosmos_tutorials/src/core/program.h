#ifndef __CORE_PROGRAM__
#define __CORE_PROGRAM__

#include <Arduino.h>

class Program {
public:
  Program();
  virtual void Setup();
  virtual void Run();
  virtual void Loop();
  virtual void Reset();
  virtual void OnKeyDown(int key);
  virtual void OnKeyUp(int key);
  virtual void OnGateIn(uint8_t channel);
  void SetNext(Program *program);
  Program *GetNext();
private:
  Program *_next;
};

#endif
