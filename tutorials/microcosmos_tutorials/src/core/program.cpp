#include "program.h"

Program::Program() {
  _next = NULL;
}

void Program::Setup() {
}

void Program::Run() {
}

void Program::Loop() {
}

void Program::Reset() {
}

void Program::OnKeyDown(int key) {
}

void Program::OnKeyUp(int key) {
}

void Program::OnGateIn(uint8_t channel) {

}

void Program::SetNext(Program *program) {
    _next = program;
}

Program *Program::GetNext() {
  return _next;
}
