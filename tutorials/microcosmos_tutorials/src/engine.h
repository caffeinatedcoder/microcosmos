#ifndef __SUB_ORBITAL_ENGINE__
#define __SUB_ORBITAL_ENGINE__

#include "../defines.h"
#ifdef MICROCOSMOS

#include <Audio.h>
#include <Arduino.h>
#include <Bounce2.h>

#include "./core/_include.h"
#include "./io/_include.h"
#include "./ui/_include.h"
#include "./utils/_include.h"

class Engine {
public:
  //singleton pattern
  static Engine *Instance();
  //end singleton pattern

  void Setup();
  void Loop();
  void OnKeyDown();

  ProgramSwitcher *programSwitcher;

  // board inputs
  Button *buttons[BUTTONS];
  Button *encoderButtons[ENCODERS];
  Pot *pots[POTS];
  Encoder *encoders[ENCODERS];
  // board display
  Oled *oled;
  Led *buttonLeds[BUTTONS_LEDS];
  Led *modeLeds[MODE_LEDS];
  Led *ioLeds[IO_LEDS];

private:
  // singleton pattern
  static Engine* _instance;
  Engine();
  ~Engine();
  Engine(const Engine&);
  Engine& operator=(const Engine&);
  // end singleton pattern

  static void encoderAInterruptCallback();
  static void encoderBInterruptCallback();
  static void encoderCInterruptCallback();

  static void buttonAInterruptCallback();
  static void buttonBInterruptCallback();
  static void buttonCInterruptCallback();
  static void buttonDInterruptCallback();

  static void encoderButtonAInterruptCallback();
  static void encoderButtonBInterruptCallback();
  static void encoderButtonCInterruptCallback();

  bool _state;
  Timer *_mainTimer;
};

#endif
#endif
