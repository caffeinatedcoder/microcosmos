#include "button.h"

Button::Button() {
}

void Button::Setup(uint8_t pin, voidFuncPtr interruptCallback) {
  _pin = pin;
  pinMode(_pin, INPUT_PULLUP);
  _lastClick = millis();
  _pressed = false;

  if (interruptCallback) {
    attachInterrupt(digitalPinToInterrupt(_pin), interruptCallback, CHANGE);
  }
}

bool Button::Dispatch() {
  bool pressed = digitalRead(_pin) == LOW;

  if (pressed == _pressed) {
    return false;
  } else {
    unsigned long current = millis();
    unsigned long elapsed = current - _lastClick;

    if (elapsed < 150) {
      _pressed = false;
      return false;
    }

    _lastClick = current;
    _pressed = pressed;
    return _pressed;
  }
}
