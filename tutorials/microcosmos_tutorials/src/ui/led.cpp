#include "led.h"

Led::Led() {
}

void Led::Setup(int pin, bool isDigital) {
	_pin = pin;
	_analogValue = 0;
	_isDigital = isDigital;
	pinMode(_pin, OUTPUT);
}

void Led::Reset() {
  _analogValue = 0;
}

void Led::Set(bool value) {
	_analogValue = value ? 255 : 0;
}

void Led::Set(int value) {
  _analogValue = value <= 255 ? value : 255;
}

void Led::Show() {
	if (_isDigital) {
		digitalWrite(_pin, _analogValue > 0 ? HIGH : LOW);
	} else {
		analogWrite(_pin, _analogValue);
	}

}
