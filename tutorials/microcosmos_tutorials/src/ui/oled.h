#ifndef __UI_OLED__
#define __UI_OLED__

#include "../../defines.h"
#if defined(SUB_ORBITAL) || defined(MICROCOSMOS)

#define FONT_WIDTH 6
#define LINE_HEIGHT 10

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "../utils/_include.h"

#include "logo.h"

class Oled {
public:
  Oled();
  void Setup();
  void ClearDisplay();
  void Render();
  void RenderStartupScreen();
  void DrawString(const char *string, int16_t x, int16_t y);
  void DrawChar(uint8_t c, int16_t x, int16_t y);

  void DrawStringLine(uint8_t line, const char *text);
  void DrawParamLine(uint8_t line, const char *name, int value, int column = -1);

  void DrawChart(int min, int max);
  void DrawPixel(int x, int y);
  void DrawRectangle(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, bool filled);
  void DrawCircle(uint16_t x, uint16_t y, uint16_t radius);
  void DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, bool dotted);

  void DrawAdsr(int a, int d, int s, int r, int x, int y, int width, int height);
  void DrawFunction(float freq, int x0, int y0, int width, int height);

  void PushChartValue(int val);
  void SetScale(int scale);
private:
  int _scale;
  int *_chart;
  int _chartPos;
  Adafruit_SSD1306 *_display;
};

#endif
#endif
