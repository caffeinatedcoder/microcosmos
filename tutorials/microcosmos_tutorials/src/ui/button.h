#ifndef __UI_BUTTON__
#define __UI_BUTTON__

#include "../../defines.h"
#include <Arduino.h>

/*
PLEASE READ:
https://create.arduino.cc/projecthub/Svizel_pritula/10-buttons-using-1-interrupt-2bd1f8
*/

class Button {
public:
  Button();
  void Setup(uint8_t pin, voidFuncPtr interruptCallback = NULL);
  bool Dispatch();
private:
  uint8_t _pin;
  unsigned long _lastClick;
  bool _pressed;
};

#endif
