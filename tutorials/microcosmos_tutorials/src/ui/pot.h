#ifndef __FASE_LUNARE_CORE_POT__
#define __FASE_LUNARE_CORE_POT__

#include <Arduino.h>
#include "../../defines.h"
#include "../utils/_include.h"

class Pot {
public:
  Pot();
  void Setup(uint8_t pin, bool inverted, int minRef, int maxRef);
  int ReadAnalog();
  int ReadRaw();
  int ReadAsUnipolar();
  int ReadAsBipolar();
  int ReadAs128();
  int ReadAs64();
  int ReadAs16();
  int ReadAs8();
  int ReadAs4();
  int GetMax();
private:
  uint8_t _pin;
  int _ema;
  int _delta;
  int _max;
  int _minRef;
  int _maxRef;
  bool _inverted;
};

#endif
