#include "oled.h"
#if defined(SUB_ORBITAL) || defined(MICROCOSMOS)

Oled::Oled() {
  _display = new Adafruit_SSD1306 (SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET_PIN);
}

void Oled::Setup() {
  _display->begin(SSD1306_SWITCHCAPVCC, 0x3C);

  _chartPos = 0;
  int chart[SCREEN_WIDTH];

  for (int i=0; i<SCREEN_WIDTH; i++) {
    chart[i] = 0;
  }

  _chart = chart;
  _scale = 1;
}

void Oled::RenderStartupScreen() {
  _display->setTextSize(1);
  _display->setCursor(30, 0);
  _display->println("MICROCOSMOS");

  for (int x = 0; x < LOGO_WIDTH; x++) {
    for (int y = 0; y < LOGO_HEIGHT; y++) {
      if (LOGO[(y*LOGO_WIDTH)+x] == 1) {
        _display->drawPixel(x+38, y+22, SSD1306_WHITE);
      }
    }
  }
}

void Oled::ClearDisplay() {
  _display->clearDisplay();
  _display->setTextColor(SSD1306_WHITE);
  _scale = 1;
}

void Oled::Render() {
  _display->display();
}

void Oled::DrawString(const char *string, int16_t x, int16_t y) {
  _display->setTextSize(_scale);
  _display->setCursor(x, y);
  _display->println(string);
}

void Oled::DrawChar(uint8_t c, int16_t x, int16_t y) {
  _display->setTextSize(_scale);
  _display->setCursor(x, y);
  _display->println(c);
}

void Oled::DrawStringLine(uint8_t line, const char *text) {
  _display->setTextSize(_scale);
  _display->setCursor(0, line*(LINE_HEIGHT*_scale));
  _display->println(text);
}

void Oled::DrawParamLine(uint8_t line, const char *name, int value, int column) {
  int sw = SCREEN_WIDTH;
  int xOffset = 0;
  int columnSpace = 5;

  if (column > -1) {
    sw = sw / 2;
    xOffset = column * sw;
  }

  int fw = (FONT_WIDTH * _scale);
  if (value > 9 && value < 100) {
    fw *= 2;
  } else if (value >= 100 && value < 1000) {
    fw *= 3;
  } else if (value >= 1000 && value < 10000) {
    fw *= 4;
  } else if (value < 0 && value > -10) {
    fw *= 2;
  } else if (value <= -10 && value > -100) {
    fw *= 3;
  } else if (value <= -100 && value > -1000) {
    fw *= 4;
  }

  int x = xOffset + sw - fw;

  _display->setCursor(x, line * LINE_HEIGHT * _scale);
  _display->println(value);

  _display->setTextSize(_scale);
  _display->setCursor(xOffset + ((column > 0) ? columnSpace : 0), line * LINE_HEIGHT * _scale);
  _display->println(name);
}

void Oled::DrawChart(int min, int max) {
  int y = 0;
  int yOffset = LINE_HEIGHT * _scale;
  int chartHeight = SCREEN_HEIGHT - yOffset;

  // draw chart data
  for (int x = 0; x < SCREEN_WIDTH; x++) {

    //y = (y + 1) % (SCREEN_HEIGHT - 22);

    y = chartHeight - map(_chart[x], min, max, 0, chartHeight);
    _display->drawPixel(x, y+yOffset, SSD1306_WHITE);
  }

  // draw vertical line
  for (y = 0; y < chartHeight; y++) {
    _display->drawPixel(_chartPos, y+yOffset, SSD1306_WHITE);
  }
}

void Oled::DrawPixel(int x, int y) {
  _display->drawPixel(x, y, SSD1306_WHITE);
}

void Oled::DrawRectangle(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, bool filled) {
  if (!filled) {
    _display->drawRect(x0, y0, w, h, SSD1306_WHITE);
  } else {
    _display->fillRect(x0, y0, w, h, SSD1306_WHITE);
  }
}

void Oled::DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, bool dotted = false) {
  if (!dotted) {
    _display->drawLine(x0, y0, x1, y1, SSD1306_WHITE);
  } else {
    // x-x0 / x1-x2 = y-y0 / y2-y1
    // x-x0 = ((y-y0) / (y2-y1)) * (x1-x2)
    // x = (((y-y0) / (y2-y1)) * (x1-x2)) + x0
    // ---
    // y-y0 = ((x-x0) / (x1-x2)) * (y2-y1)
    // y = ((x-x0) / (x1-x2)) * (y2-y1) + y0

    int dash = 3;

    if (x0 == x1) {
      for (int y = y0; y < y1; y ++) {
        if (y % dash) {
          DrawPixel(x0, y);
        }
      }

      return;
    }

    if (y0 == y1) {
      for (int x = x0; x < x1; x ++) {
        if (x % dash) {
          DrawPixel(x, y0);
        }
      }

      return;
    }

    // y = m * x
    _display->drawLine(x0, y0, x1, y1, SSD1306_WHITE);

  }

}

void Oled::DrawAdsr(int a, int d, int s, int r, int x, int y, int width, int height) {
  int max = 128 * 3;
  int ax = x + a*width/max;
  int dx = x + ax + d*width/max;
  int dy = y + (height - s*height/128);
  int rx = x + dx + r*width/max;
  DrawLine(x, y + height, ax, y, true);
  DrawLine(ax, y, dx, dy);
  DrawLine(dx, dy, rx, y + height);

  DrawLine(ax, y, ax, y + height, true);
  DrawLine(dx, dy, dx, y + height, true);
}

void Oled::DrawFunction(float freq, int x0, int y0, int width, int height) {
  int a = height / 2;
  float h = 0.0;

  float p = 0;
  int y = a * sin( (p - h) / freq ) + a;

  int p0x = x0;
  int p0y = y + y0;

  for (int x = 0; x < width; x++) {
    float p = PI * x / width;
    int y = a * sin( (p - h) / freq ) + a;
    DrawLine(p0x, p0y, x + x0, y + y0);
    p0x = x + x0;
    p0y = y + y0;
  }
}

void Oled::DrawCircle(uint16_t x, uint16_t y, uint16_t radius) {
  _display->drawCircle(x, y, radius, SSD1306_WHITE);
}

void Oled::PushChartValue(int val) {
  _chartPos = (_chartPos + 1) % SCREEN_WIDTH;
  _chart[_chartPos] = val;
}

void Oled::SetScale(int scale) {
  _scale = scale;
}

#endif
