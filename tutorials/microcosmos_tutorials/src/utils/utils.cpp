#include "utils.h"

int exponential_moving_average(int value, int last) {
  return (EMA_ALPHA*value) + ((1-EMA_ALPHA)*last);
}

int bpm_to_time(int bpm, int numerator, int denominator) {
  float division = (numerator*1.)/denominator;
	return static_cast<int>(ONE_SECOND/bpm/(1./4./division));
}

float m_to_f(int note) {
  //return /*static_cast<int>*/(A440 / 32.) * (2 ^ ((note - 9) / 12));
  return 440.*(pow(2., ((note-69.)/12.)));
}

float func_linear(int x, int w, int h) {
  // JS
  // return x * h / w;
  return x * h / w;
}

float func_inverse_linear(int x, int w, int h) {
  // JS
  // return h - (x * h / w);
  return h - (x * h / w);
}

float func_exp(int x, int beta, int w, int h) {
  // JS
  // return h - (beta * (-1 + Math.exp( (x / w) * Math.log(1 + (h / beta))) ));
  return h - (beta * (-1 + exp( (x / w) * log(1 + (h / beta))) ));
}

float func_log(int x, int beta, int w, int h) {
  // JS
  // return beta * (-1 + Math.exp( ((w-x) / w) * Math.log(1 + (h / beta))) );
  return beta * (-1 + exp( ((w-x) / w) * log(1 + (h / beta))) );
}

float func_slope(int x, int step, int r, int w, int h) {
  // JS
  // return h - (h * (0.5 + ( (Math.atan( (x - step) / r ) ) / Math.PI )));
  return h - (h * (0.5 + ( (atan( (x - step) / r ) ) / PI )));
}

void text_file_save(const char *fileName, const char *data) {
  SD.remove(fileName);
  File file = SD.open(fileName, FILE_WRITE);
  if (file) {
    file.write(data);
    file.close();
  } else {
    Serial.println("error saving file");
  }
}

const char *text_file_load(const char *fileName) {
  File file = SD.open(fileName);
  if (file) {
    unsigned long fSize = file.size();
    char *buffer = (char*) malloc(sizeof(char)*fSize);
    file.read(buffer, fSize);
    return buffer;
  } else {
  	// if the file didn't open, print an error:
    Serial.println("error reading file");
    return "";
  }
}

void eeprom_save(int address, int value) {
  EEPROM.put(address, value);
}

int eeprom_load(int address) {
  return EEPROM.read(address);
}

void eeprom_clear(int address) {

}
