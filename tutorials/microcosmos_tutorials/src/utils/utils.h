#ifndef __UTILS__
#define __UTILS__

#include "../../defines.h"
#include <Arduino.h>
#include <SPI.h>
#include <SD.h>
#include <EEPROM.h>

int exponential_moving_average(int value, int last);
int bpm_to_time(int bpm, int numerator, int denominator);
float m_to_f(int note);
float func_linear(int x, int w, int h);
float func_inverse_linear(int x, int w, int h);
float func_exp(int x, int beta, int w, int h);
float func_log(int x, int beta, int w, int h);
float func_slope(int x, int step, int r, int w, int h);

void text_file_save(const char *fileName, const char *data);
const char *text_file_load(const char *fileName);

void eeprom_save(int address, int value);
int eeprom_load(int address);
void eeprom_clear(int address);

#endif
