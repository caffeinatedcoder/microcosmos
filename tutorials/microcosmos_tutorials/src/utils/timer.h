#ifndef CORE_TIMER
#define CORE_TIMER

#include <Arduino.h>

class Timer {
public:
  Timer();
  void Setup();
  unsigned long GetElapsed();
  void Update();

  // TODO: from python
  /*
  bpmToTime(bpm,numerator,denominator);
  bpmToSamples(bpm,numerator,denominator,rate);
  timeToBpm(time,numerator,denominator);
  timeToSamples(time,rate);
  samplesToTime(samples,rate);
  samplesToBpm(samples,rate,numerator,denominator);
  */
private:
  unsigned long last;
  unsigned long current;
};

#endif
