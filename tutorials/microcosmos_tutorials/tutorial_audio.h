#ifndef __TUTORIAL_AUDIO__
#define __TUTORIAL_AUDIO__

#include <Arduino.h>
#include "src/engine.h"
#include "src/utils/_include.h"

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

class TutorialAudio : public Program {
public:
  TutorialAudio();
  void Setup();
  void Loop();
  void Run();
  void OnKeyDown(int key);
private:
  int a, b, c;
  uint8_t waveform;
};

#endif
