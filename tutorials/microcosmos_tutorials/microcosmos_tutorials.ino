#include <Audio.h>
#include "src/engine.h"
#include "src/utils/_include.h"
#include "tutorial_program.h"
#include "tutorial_audio.h"
// #include "tutorial_faust.h"

Engine *engine = Engine::Instance();

void setup() {
  Serial.begin(9600);
  Serial.println("OK");

  //SD.begin(SDCARD_CS_PIN);

  AudioMemory(40);
  AudioInterrupts();

  engine->Setup();
  engine->programSwitcher->PushProgram(new TutorialProgram());
  engine->programSwitcher->PushProgram(new TutorialAudio());
  //engine->programSwitcher->PushProgram(new TutorialFaust());
}

void loop() {
  engine->Loop();
}
