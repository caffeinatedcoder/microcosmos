#ifndef __TUTORIAL_FAUST__
#define __TUTORIAL_FAUST__

#include <Arduino.h>
#include "src/engine.h"
#include "src/utils/_include.h"
#include "src/faust/voice.h"


class TutorialFaust : public Program {
public:
  TutorialFaust();
  void Setup();
  void Loop();
  void Run();
  void OnKeyDown(int key);
private:
};

#endif
