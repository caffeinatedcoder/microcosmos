#include "tutorial_program.h"

TutorialProgram::TutorialProgram() : Program() {
  Setup();
}

void TutorialProgram::Setup() {
}

void TutorialProgram::Run() {
}

void setPotParameter(int potNumber, int low, int high, const char *paramName) {
}

void TutorialProgram::Loop() {
  Engine::Instance()->modeLeds[0]->Set(5);

  Engine::Instance()->oled->SetScale(2);
  Engine::Instance()->oled->DrawStringLine(0, "THIS IS A");
  Engine::Instance()->oled->DrawStringLine(1, "TUTORIAL");
  Engine::Instance()->oled->DrawStringLine(2, "0123456789");
}

void TutorialProgram::OnKeyDown(int key) {
  if (key < 4) {
    Engine::Instance()->buttonLeds[key]->Set(5);
  } else if (key == 4) {
    Engine::Instance()->programSwitcher->NextProgram();
  }
}
