#ifndef __TUTORIAL_PROGRAM__
#define __TUTORIAL_PROGRAM__

#include "src/engine.h"
#include "src/utils/_include.h"

class TutorialProgram : public Program {
public:
  TutorialProgram();
  void Setup();
  void Loop();
  void Run();
  void OnKeyDown(int key);
private:
};

#endif
