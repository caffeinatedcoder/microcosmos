
/* Microcosmos development board for Teensy 3.6 
 *  Author: FaseLunare
 *
 * 
 * 
 * DEFINE HARDWARE PINS
 */

// Button input pins Teensy  3.6 left to right

#define SW1 39
#define SW2 34
#define SW3	33
#define SW4	32

//  Led pin 
// UNDER TEENSY FROM LEFT TO RIGHT LEDA...LEDD

#define LEDA	 6
#define LEDB   9
#define LEDC   29 
#define LEDD   30

// BUTTON LEDS 

#define LEDFN	38
#define LED1  37
#define LED2  36
#define LED3  35

// CONNECTOR LEDS

#define LED4			20
#define LED5  	  23
#define LED6	    22
#define LED7			21


//  POT1 TO POT3 FROM LEFT TO RIGHT

#define POT1       A10
#define POT2       15 
#define POT3       14 


//  ENCODER1 TO ENCODER3 FROM LEFT TO RIGHT (SWITCH SECTION)

#define ENC1SW       7
#define ENC2SW       8 
#define ENC3SW       28
 

//  ENCODER1 TO ENCODER3 FROM LEFT TO RIGHT (QUADRATURE SECTION)

#define ENC1A       2
#define ENC1B       3

#define ENC2A       4
#define ENC2B       5

#define ENC3A       24
#define ENC3B       25

//  MIDI INPUT&OUTPUT

#define MIDI_IN		0
#define MIDI_OUT	1

//  STEREO AUDIO IN

#define AUDIOIN1	16
#define AUDIOIN2	17

//  STEREO AUDIO OUT

#define AUDIOOUT1	A21
#define AUDIOOUT2	A22

//  I2C BUS

#define SCL			19
#define SDA			18

//  SPI0 BUS & CS PIN

#define MOSI0		11
#define MISO0		12
#define SCK0		13
#define CS0			10
#define CS1			31

//  SPI2 BUS

#define MOSI2		44
#define MISO2		45
#define SCK2		46
#define CS2			43

//  USB HOST PORT ON "A" TYPE CONNECTOR J4

#define D+			A25
#define D-			A26
