/*  Faselunare - Microcosmos development board for Teensy 3.6
 *  Author: FaseLunare
 *  Faselunare 04/2020
 *
 *  https://www.faselunare.com/tutorials
 *
 *  Microcosmos is equipped with 3 potentiometers the bottom row
 *
 *  This example includes the microcosmos.h file which contains the hardware definition.
 *  According to this file, the POTS are thus addressed
 *
 *  POT1 PO2 POT3
 *
 *  analogRead
 *  Reads an analog input and prints the result to the Serial Monitor.
 *  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
 *
 */

#include "microcosmos.h"

void setup() {
    Serial.begin(9600); // initialize serial communication at 9600 bits per second
    Serial.println(POT1);
    Serial.println(POT2);
    Serial.println(POT3);
}


void loop() {


    int P1 = analogRead(POT1);
    int P2 = analogRead(POT2);
    int P3 = analogRead(POT3);

    Serial.print(P1);
    Serial.print("\t");
    Serial.print(P2);
    Serial.print("\t");
    Serial.print(P3);
    Serial.println();

    delay(10);        // delay in between reads for stability
}
