#include "USBHost_t36.h"

USBHost myusb;
USBHub hub1(myusb);
USBHub hub2(myusb);
USBHub hub3(myusb);
MIDIDevice midi1(myusb);

void setup() {
  myusb.begin();
  midi1.setHandleNoteOff(onNoteOff);
  midi1.setHandleNoteOn(onNoteOn);
  midi1.setHandleControlChange(onControlChange);
}

void loop() {
  midi1.read();
}

void onNoteOn(byte channel, byte note, byte velocity) {
  // do something...
  Serial.print("Note On, ch=");
	Serial.print(channel);
	Serial.print(", note=");
	Serial.print(note);
	Serial.print(", velocity=");
	Serial.print(velocity);
	Serial.println();
}

void onNoteOff(byte channel, byte note, byte velocity) {
  // do something...
}

void onControlChange(byte channel, byte control, byte value) {
  // do something...
  Serial.print("Control Change, ch=");
	Serial.print(channel);
	Serial.print(", control=");
	Serial.print(control);
	Serial.print(", value=");
	Serial.print(value);
	Serial.println();
}
