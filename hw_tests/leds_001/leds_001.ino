/*  Faselunare - Microcosmos development board for Teensy 3.6 
 *  Author: FaseLunare
 *  Faselunare 04/2020 
 *  
 *  https://www.faselunare.com/tutorials
 *  
 *  Microcosmos is equipped with 12 single-color LEDs
 *  They are positioned in 2 rows, bottom and top.
 *  This example includes the microcosmos.h file which contains the hardware definition.
 *  
 *  According to this file, the LEDs are thus addressed
 *  Bottom row, from left to right
 *  
 *  LEDA LEDB LEDC LEDD    LEDFN LED1 LED2 LED3    
 *  
 *  Top row, from left to right
 *  
 *  LED4 LED5 LED6 LED7
 *  
 *  Leds can be controlled with digitalWrite and analogWrite
 *  
 *  digitalWrite
 *  If the pin has been configured as an OUTPUT with pinMode(), 
 *  its voltage will be set to 3.3V for HIGH, 0V (ground) for LOW
 *  
 *  analogWrite
 *  If the pin has been configured as an OUTPUT with pinMode(),
 *  writes an analog value (PWM wave) to a pin. Can be used to light a LED at varying brightnesses.
 * 
 */

#include "microcosmos.h"

void setup() {
    pinMode(LEDA, OUTPUT);
    pinMode(LEDB, OUTPUT);
    pinMode(LEDC, OUTPUT);
    pinMode(LEDD, OUTPUT);

    pinMode(LEDFN,OUTPUT);
    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
    pinMode(LED3, OUTPUT);

    pinMode(LED4, OUTPUT);
    pinMode(LED5, OUTPUT);
    pinMode(LED6, OUTPUT);
    pinMode(LED7, OUTPUT);
}


void loop() {
    digitalWrite(LEDA, HIGH);         // set the LED on, full brightness
    digitalWrite(LEDB, HIGH);         // set the LED on, full brightness
    digitalWrite(LEDC, HIGH);         // set the LED on, full brightness
    digitalWrite(LEDD, HIGH);         // set the LED on, full brightness
    delay(500);                       // wait for a second
    digitalWrite(LEDA, LOW);          // set the LED off
    digitalWrite(LEDB, LOW);          // set the LED off
    digitalWrite(LEDC, LOW);          // set the LED off
    digitalWrite(LEDD, LOW);          // set the LED off
    delay(500);                       // wait for a second

    analogWrite(LEDFN, 5);            // set the LED on, brightness set to 10
    delay(500);                       // wait for a second
    analogWrite(LEDFN, 0);            // set the LED off
    delay(500); 

    analogWrite(LED1, 8);             // set the LED on, brightness set to 20
    delay(500);                       // wait for a second
    analogWrite(LED1, 0);             // set the LED off
    delay(500); 

    analogWrite(LED2, 12);            // set the LED on, brightness set to 40
    delay(500);                       // wait for a second
    analogWrite(LED2, 0);             // set the LED off
    delay(500); 

    analogWrite(LED3, 15);            // set the LED on, brightness set to 80
    delay(500);                       // wait for a second
    analogWrite(LED3, 0);             // set the LED off
    delay(500); 

    
    for (int i = 0; i <= 255; i++) {  // Top leds fadein
      analogWrite(LED4, i);
      analogWrite(LED5, i);
      analogWrite(LED6, i);
      analogWrite(LED7, i);
      delay(50);
    } 

    for (int i = 255; i >= 0; i--) {  // Top leds fadeout
      analogWrite(LED4, i);
      analogWrite(LED5, i);
      analogWrite(LED6, i);
      analogWrite(LED7, i);
      delay(10);
    } 

}
