/*  Faselunare - Microcosmos development board for Teensy 3.6 
 *  Author: FaseLunare
 *  Faselunare 04/2020 
 *  DISPLAY 001 - Check the display and write on it!
 *  
 *  https://www.faselunare.com/tutorials
 *  
 *  On the Microcosmos the OLED display is connected on the I2C bus with the address 0x3C
 *  Dimensions is 128x64 pixels and use the Adafruit library (but you can use also the U8g2 library
 *  
 *  Common drawing functions
 *  display.clearDisplay() – all pixels are off
 *  display.drawPixel(x,y, color) – plot a pixel in the x,y coordinates
 *  display.setTextSize(n) – set the font size, supports sizes from 1 to 8
 *  display.setCursor(x,y) – set the coordinates to start writing text
 *  display.print(“message”) – print the characters at location x,y
 *  display.display() – call this method for the changes to make effect
 *  
 *  
 */

#include <Wire.h>                                                               // This library allows you to communicate with I2C OLED Display
#include <Adafruit_GFX.h>                                                       // Adafruit graphic library
#include <Adafruit_SSD1306.h>                                                   // Adafruit SSD1306 library

#define SCREEN_WIDTH 128                                                        // OLED display width, in pixels
#define SCREEN_HEIGHT 64                                                        // OLED display height, in pixels

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);               // Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
  
void setup() {
  Serial.begin(9600);
  
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {                              // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally - Address 0x3C for Microcosmos
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);                                                                    // Don't proceed, loop forever
  }

  display.clearDisplay();
}


void loop() {
  display.clearDisplay();                                                       // Clear the buffer
  display.setTextSize(1);                                                       // Normal 1:1 pixel scale (value from 1 to 8)
  display.setTextColor(WHITE);                                                  // Draw white text
  display.setCursor(0,0);                                                       // Start at top-left corner
  display.println("FASELUNARE - MICROCOSMOS");                                  // The println("...") method prints the string "..." and moves the cursor to a new line.
  display.print("FASELUNARE - MICROCOSMOS ");                                    // The print("...") method instead prints just the string "...", but does not move the cursor to a new line
  display.print("FASELUNARE - MICROCOSMOS");
  display.display();                                                            // Render the content of the buffer   
} 
