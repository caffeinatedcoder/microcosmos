import("stdfaust.lib");

lfofeq = hslider("lfo_freq", 1, 0.1, 8.0, 0.1):si.smoo;
centerfeq = hslider("center_freq", 440, 80, 5000, 1):si.smoo;

lfo = os.lf_saw(lfofeq);
sine1 = os.oscs(centerfeq * 2 * lfo);
sine2 = os.oscs(centerfeq);

process = sine1, sine2 > _;
