import("stdfaust.lib");

map(x, in_min, in_max, out_min, out_max) = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

// tableSize2 = hslider("[0]table size", 1, 220, 480, 1) * 100; //48000;
tableSize = 48000;
recIndex = (+(1) : %(tableSize)) ~ *(record);
readIndex = readSpeed/float(ma.SR) : (+ : ma.decimal) ~ _ : *(float(tableSize)) : int;
//readSpeed = hslider("[0]Read Speed", 1, -3, 5, 0.1);
readSpeed = map(hslider("read_speed", 16, 0, 128, 1) : si.smoo, 0, 128, -3, 5);

record = button("record") : int;
looper = rwtable(tableSize,0.0,recIndex,_,readIndex);
process = looper;
