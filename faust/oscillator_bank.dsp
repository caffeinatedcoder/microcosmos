import("stdfaust.lib");

pitchRangeLow = 80;
pitchRangeHi = 440;

waveSelectA = hslider("wave_select_a", 0, 0, 4, 1);
pitchA = hslider("pitch_a", 440, pitchRangeLow, pitchRangeHi, 1) : si.smoo;
dutyA = hslider("duty_a", 0.5, 0.0, 0.95, 0.01) : si.smoo;
gainA = hslider("gain_a", 0.5, 0.0, 0.95, 0.01) : si.smoo;

waveSelectB = hslider("wave_select_b", 0, 0, 4, 1);
pitchB = hslider("pitch_b", 440, pitchRangeLow, pitchRangeHi, 1) : si.smoo;
dutyB = hslider("duty_b", 0.5, 0.0, 0.95, 0.01) : si.smoo;
gainB = hslider("gain_b", 0.5, 0.0, 0.95, 0.01) : si.smoo;

waveSelectSub = hslider("wave_select_sub", 0, 0, 4, 1);
dutySub = hslider("duty_sub", 0.5, 0.0, 0.95, 0.01) : si.smoo;
gainSub = hslider("gain_sub", 0.0, 0.0, 0.95, 0.01) : si.smoo;

// sine, square, saw, tri, noise

osc(pitch, waveSelect, duty, gain) = (
  os.osc(pitch),
  os.pulsetrain(pitch, duty),
  os.sawtooth(pitch),
  os.triangle(pitch),
  no.noise : ba.selectn(5, waveSelect) * gain
);

oscillators = osc(pitchA, waveSelectA, dutyA, gainA), osc(pitchB, waveSelectB, dutyB, gainB) :> _;
suOscillator = osc(pitchA / 2, waveSelectSub, dutySub, gainSub);


process =  oscillators, suOscillator :> _;
