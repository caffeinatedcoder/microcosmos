import("stdfaust.lib");

beat = os.lf_pulsetrainpos(2, 0.5);
gate = button ("gate");

freq = hslider("FREQ",440,110,880,1);
volA = hslider("A",0.01,0.01,4,0.01);
volDR = hslider("DR",0.6,0.01,8,0.01);
volS = hslider("S",0.2,0,1,0.01);
fmAmount = hslider("FM",0,0,5000,1);
envelop = en.adsre(volA,volDR,volS,volDR,beat);

op1 = os.osc(freq) * envelop;
op2 = os.osc(freq + (op1 * fmAmount)) * envelop;
op3 = os.osc(freq + (op2 * fmAmount)) * envelop;


process = op3;
