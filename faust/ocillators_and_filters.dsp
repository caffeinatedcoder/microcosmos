import("stdfaust.lib");

pitchRangeLow = 80;
pitchRangeHi = 440;
coRangeLow = 50;
coRangeHi = 10000;

waveSelectA = hslider("osc_a_wave_select", 0, 0, 4, 1);
pitchA = hslider("osc_a_pitch", 440, pitchRangeLow, pitchRangeHi, 1) : si.smoo;
dutyA = hslider("osc_a_duty", 0.5, 0.0, 0.95, 0.01) : si.smoo;
gainA = hslider("osc_a_gain", 0.0, 0.0, 0.95, 0.01) : si.smoo;

waveSelectB = hslider("osc_b_wave_select", 0, 0, 4, 1);
pitchB = hslider("osc_b_pitch", 440, pitchRangeLow, pitchRangeHi, 1) : si.smoo;
dutyB = hslider("osc_b_duty", 0.5, 0.0, 0.95, 0.01) : si.smoo;
gainB = hslider("osc_b_gain", 0.5, 0.0, 0.95, 0.01) : si.smoo;

waveSelectSub = hslider("osc_c_wave_select", 0, 0, 4, 1);
dutySub = hslider("osc_c_duty", 0.5, 0.0, 0.95, 0.01) : si.smoo;
gainSub = hslider("osc_c_gain", 0.5, 0.0, 0.95, 0.01) : si.smoo;

cutoffA = hslider("filter_a_cutoff", 500, coRangeLow, coRangeHi, 0.01) : si.smoo;
resonanceA = hslider("filter_a_resonance", 5, 1, 30, 0.1) : si.smoo;
modeA = hslider("filter_a_mode", 0, 0, 3, 1);

cutoffB = hslider("filter_b_cutoff", 500, coRangeLow, coRangeHi, 0.01) : si.smoo;
resonanceB = hslider("filter_b_resonance", 5, 1, 30, 0.1) : si.smoo;
modeB = hslider("filter_b_mode", 0, 0, 3, 1);

filterRouting = hslider("filter_routing", 0, 0, 1, 1);

lfo1Freq = hslider("lfo_1_freq", 1, 0.1, 8.0, 0.1):si.smoo;
lfo1Wav = hslider("lfo_1_wave", 0, 0, 2, 1);

lfo2Freq = hslider("lfo_2_freq", 1, 0.1, 8.0, 0.1):si.smoo;
lfo2Wav = hslider("lfo_2_wave", 0, 0, 2, 1);

// sine, square, saw, tri, noise
osc(pitch, waveSelect, duty, gain) = (
  os.osc(pitch),
  os.pulsetrain(pitch, duty),
  os.sawtooth(pitch),
  os.triangle(pitch),
  no.noise : ba.selectn(5, waveSelect) * gain
);

// lp, bp, hp
filter(cutoff, resonance, mode, gain) = (
  fi.resonlp(cutoff, resonance, 0.9),
  fi.resonbp(cutoff, resonance, 0.9),
  fi.resonhp(cutoff, resonance, 0.9) : ba.selectn(3, mode) * gain
);

lfo(freq, waveSelect) = (
  os.lf_triangle(freq),
  os.lf_saw(freq),
  os.lf_squarewavepos(freq): ba.selectn(3, waveSelect)
);

lfo1 = lfo(lfo1Freq, lfo1Wav);
lfo2 = lfo(lfo2Freq, lfo2Wav);

env1 = adsr(a,d,s,r,t);
env2 = adsr(a,d,s,r,t);

oscillatorA = osc(pitchA, waveSelectA, dutyA, gainA);
oscillatorB = osc(pitchB, waveSelectB, dutyB, gainB);
suOscillator = osc(pitchA / 2, waveSelectSub, dutySub, gainSub);

oscillatorsMix = oscillatorA, oscillatorB, suOscillator :> _ ;

filterA = filter(cutoffA, resonanceA, modeA, 1.0);
filterB = filter(cutoffB, resonanceB, modeB, 1.0);

singleFilter = oscillatorsMix <: _,_,_ : filterA;
routingSerial = oscillatorsMix <: _,_,_ : filterA <: _,_,_ : filterB;
routingParallel = (oscillatorsMix <: _,_,_ : filterA), (oscillatorsMix <: _,_,_ : filterB) :> _;

process = singleFilter, routingSerial, routingParallel : ba.selectn(3, filterRouting);
