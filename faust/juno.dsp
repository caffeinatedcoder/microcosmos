import("stdfaust.lib");

pitchRangeLow = 80;
pitchRangeHi = 440;
coRangeLow = 50;
coRangeHi = 10000;
map(x, in_min, in_max, out_min, out_max) = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

gate = button("gate");

mainPanel(x) = hgroup("MAIN PANEL", x);
oscAGroup(x) = mainPanel(hgroup("OSC", x));
lfoGroup(x) = mainPanel(hgroup("LFO", x));
filterGroup(x) = mainPanel(hgroup("VCF", x));
envGroup(x) = mainPanel(hgroup("ENV", x));

lfo1Freq = map(lfoGroup(vslider("lfo_1_freq", 16, 0, 128, 1):si.smoo), 0, 128, 0.01, 8.0);

env1A = map(envGroup(vslider("1a", 0, 0, 128, 1):si.smoo), 0, 128, 0.01, 4.5);
env1D = map(envGroup(vslider("2d", 128, 0, 128, 1):si.smoo), 0, 128, 0.01, 4.5);
env1S = map(envGroup(vslider("3s", 22, 0, 128, 1):si.smoo), 0, 128, 0.0, 1.0);
env1R = map(envGroup(vslider("4r", 30, 0, 128, 1):si.smoo), 0, 128, 0.01, 4.5);

pitch = ba.midikey2hz(oscAGroup(vslider("note", 65, 0, 128, 1)));
dutyA = map(oscAGroup(vslider("duty", 65, 0, 128, 1) : si.smoo), 0, 128, 0.01, 0.95);
dutyLfo = map(oscAGroup(vslider("duty_lfo", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
dutyEnv = map(oscAGroup(vslider("duty_env", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
pitchLfo = map(oscAGroup(vslider("pitch_lfo", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
pitchEnv = map(oscAGroup(vslider("pitch_env", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
volA = map(oscAGroup(vslider("vol_a", 64, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
volB = map(oscAGroup(vslider("vol_b", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
volC = map(oscAGroup(vslider("vol_c", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
dutyB = 0.5;

cutoff = map(filterGroup(vslider("cutoff", 64, 0, 128, 1) : si.smoo), 0, 128, coRangeLow, coRangeHi);
cutoffLfo = map(filterGroup(vslider("cutoff_lfo", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
cutoffEnv = map(filterGroup(vslider("cutoff_env", 0, 0, 128, 1) : si.smoo), 0, 128, 0.0, 1.0);
resonance = map(filterGroup(vslider("resonance", 0, 0, 128, 1) : si.smoo), 0, 128, 1, 15);

masterVolume = map(vslider("master_volume", 100, 0, 128, 1) : si.smoo , 0, 128, 0.0, 1.0);

lfo = os.lf_triangle(lfo1Freq) : lfoGroup(vbargraph("freq",-1,1));
env1 = gate : en.adsr(env1A, env1D, env1S, env1R);

dutyModulation = dutyA + dutyA * (lfo * dutyLfo) + dutyA * (env1 * dutyEnv) : oscAGroup(vbargraph("duty_view",0,1));
pitchModulation = pitch + pitch * (lfo * pitchLfo) + pitch * (env1 * pitchEnv) : oscAGroup(vbargraph("pitch_view",pitchRangeLow,pitchRangeHi));
cutoffModulation = cutoff + cutoff * (lfo * cutoffLfo) + cutoff * (env1 * cutoffEnv) : filterGroup(vbargraph("cutoff_view",coRangeLow,coRangeHi));

ocillators =
  os.pulsetrain(pitchModulation, dutyModulation) * volA,
  os.sawtooth(pitchModulation) * volB,
  os.pulsetrain(pitchModulation / 2 , dutyB) * volC :> _;

filter = fi.resonlp(cutoffModulation, resonance, 0.9);

process = ocillators : filter * env1 * masterVolume;
