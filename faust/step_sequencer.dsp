import("stdfaust.lib");

mainPanel(x) = hgroup("notes", x);
buttonPanel(x) = hgroup("on off", x);

bpm = vslider("bpm", 120, 0, 256, 1);
n1 = mainPanel(vslider("1", 41, 0, 127, 1));
n2 = mainPanel(vslider("2", 41, 0, 127, 1));
n3 = mainPanel(vslider("3", 41, 0, 127, 1));
n4 = mainPanel(vslider("4", 41, 0, 127, 1));
n5 = mainPanel(vslider("5", 41, 0, 127, 1));
n6 = mainPanel(vslider("6", 41, 0, 127, 1));
n7 = mainPanel(vslider("7", 41, 0, 127, 1));
n8 = mainPanel(vslider("8", 41, 0, 127, 1));

t1 = buttonPanel(vslider("t1", 0.5, 0.0, 1.0, 0.01));
t2 = buttonPanel(vslider("t2", 0.5, 0.0, 1.0, 0.01));
t3 = buttonPanel(vslider("t3", 0.5, 0.0, 1.0, 0.01));
t4 = buttonPanel(vslider("t4", 0.5, 0.0, 1.0, 0.01));
t5 = buttonPanel(vslider("t5", 0.5, 0.0, 1.0, 0.01));
t6 = buttonPanel(vslider("t6", 0.5, 0.0, 1.0, 0.01));
t7 = buttonPanel(vslider("t7", 0.5, 0.0, 1.0, 0.01));
t8 = buttonPanel(vslider("t8", 0.5, 0.0, 1.0, 0.01));

b = ba.beat(bpm);
step = b : ba.pulse_countup(1) % 8;
note = n1, n2, n3, n4, n5, n6, n7, n8 : ba.selectn(8, step);
vol = t1, t2, t3, t4, t5, t6, t7, t8 : ba.selectn(8, step);
freq = ba.midikey2hz(note);

env = en.ar(0.01, 1);

osc = os.pulsetrain(freq, 0.5);

process = osc * vol * (step : env);
