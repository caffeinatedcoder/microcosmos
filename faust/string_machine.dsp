import("stdfaust.lib");

freq = hslider("[0]freq",220,0,1000,1);
duty = hslider("[1]duty",0.5,0,1,0.01);
detune = hslider("[2]detune",0,0,1,0.01);
gate = button("[3]gate");

freq2 = freq + (freq * detune) : vbargraph("freq2", 0, 1000);

//osc1 = os.pulsetrain(freq, duty);
//osc2 = os.pulsetrain(freq2, duty);
osc1 = os.sawtooth(freq);
osc2 = os.sawtooth(freq2);

// pf.phaser2_mono(1, 0.5);

process = (osc1 + osc2) * 0.05 : _ <: dm.phaser2_demo;
