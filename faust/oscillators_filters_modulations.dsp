import("stdfaust.lib");

pitchRangeLow = 80;
pitchRangeHi = 440;
coRangeLow = 50;
coRangeHi = 10000;

trigger = button("___trigger");

oscAGroup(x) = oscBankGroup(vgroup("OSC A", x));
oscBGroup(x) = oscBankGroup(vgroup("OSC B", x));
subGroup(x) = oscBankGroup(vgroup("SUB", x));

oscBankGroup(x) = hgroup("OSCILLATORS", x);

waveSelectA = oscAGroup(hslider("osc_a_wave_select", 0, 0, 4, 1));
pitchA = oscAGroup(hslider("osc_a_pitch", 440, pitchRangeLow, pitchRangeHi, 1) : si.smoo);
dutyA = oscAGroup(hslider("osc_a_duty", 0.5, 0.0, 0.95, 0.01) : si.smoo);
gainA = oscAGroup(hslider("osc_a_gain", 0.0, 0.0, 0.95, 0.01) : si.smoo);
pEnvA = oscAGroup(hslider("osc_a_penv", 0.0, 0.0, 0.95, 0.01) : si.smoo);
lfoA = oscAGroup(hslider("osc_a_lfo", 0.0, 0.0, 0.95, 0.01) : si.smoo);

waveSelectB = oscBGroup(hslider("osc_b_wave_select", 0, 0, 4, 1));
pitchB = oscBGroup(hslider("osc_b_pitch", 440, pitchRangeLow, pitchRangeHi, 1) : si.smoo);
dutyB = oscBGroup(hslider("osc_b_duty", 0.5, 0.0, 0.95, 0.01) : si.smoo);
gainB = oscBGroup(hslider("osc_b_gain", 0.5, 0.0, 0.95, 0.01) : si.smoo);
pEnvB = oscBGroup(hslider("osc_b_penv", 0.0, 0.0, 0.95, 0.01) : si.smoo);
lfoB = oscBGroup(hslider("osc_b_lfo", 0.0, 0.0, 0.95, 0.01) : si.smoo);

waveSelectSub = subGroup(hslider("osc_c_wave_select", 0, 0, 4, 1));
dutySub = subGroup(hslider("osc_c_duty", 0.5, 0.0, 0.95, 0.01) : si.smoo);
gainSub = subGroup(hslider("osc_c_gain", 0.5, 0.0, 0.95, 0.01) : si.smoo);

cutoffA = hslider("filter_a_cutoff", 500, coRangeLow, coRangeHi, 0.01) : si.smoo;
resonanceA = hslider("filter_a_resonance", 5, 1, 30, 0.1) : si.smoo;
modeA = hslider("filter_a_mode", 0, 0, 3, 1);

cutoffB = hslider("filter_b_cutoff", 500, coRangeLow, coRangeHi, 0.01) : si.smoo;
resonanceB = hslider("filter_b_resonance", 5, 1, 30, 0.1) : si.smoo;
modeB = hslider("filter_b_mode", 0, 0, 3, 1);

filterRouting = hslider("filter_routing", 0, 0, 1, 1);

env1A = hslider("env_1_a", 1, 0.1, 4.5, 0.1):si.smoo;
env1D = hslider("env_1_d", 1, 0.1, 4.5, 0.1):si.smoo;
env1S = hslider("env_1_s", 1, 0.0, 1.0, 0.1):si.smoo;
env1R = hslider("env_1_r", 1, 0.1, 4.5, 0.1):si.smoo;

lfo1Freq = hslider("lfo_1_freq", 1, 0.1, 8.0, 0.1):si.smoo;
lfo1Wav = hslider("lfo_1_wave", 0, 0, 2, 1);

lfo2Freq = hslider("lfo_2_freq", 1, 0.1, 8.0, 0.1):si.smoo;
lfo2Wav = hslider("lfo_2_wave", 0, 0, 2, 1);

// sine, square, saw, tri, noise
osc(pitch, waveSelect, duty, gain) = (
  os.osc(pitch),
  os.pulsetrain(pitch, duty),
  os.sawtooth(pitch),
  os.triangle(pitch),
  no.noise : ba.selectn(5, waveSelect) * gain
);

// lp, bp, hp
filter(cutoff, resonance, mode, gain) = (
  fi.resonlp(cutoff, resonance, 0.9),
  fi.resonbp(cutoff, resonance, 0.9),
  fi.resonhp(cutoff, resonance, 0.9) : ba.selectn(3, mode) * gain
);

lfo(freq, waveSelect) = (
  os.lf_triangle(freq),
  os.lf_saw(freq),
  os.lf_squarewavepos(freq): ba.selectn(3, waveSelect)
);

lfo1 = lfo(lfo1Freq, lfo1Wav);
lfo2 = lfo(lfo2Freq, lfo2Wav);

env1 = en.adsr(env1A,env1D,env1S,env1R,trigger);
// env2 = en.adsr(a,d,s,r,trigger);

pitchAMod = pitchA; // * (pEnvA * env1);

oscillatorA = osc(pitchAMod, waveSelectA, dutyA, gainA);
oscillatorB = osc(pitchB, waveSelectB, dutyB, gainB);
suOscillator = osc(pitchAMod / 2, waveSelectSub, dutySub, gainSub);

oscillatorsMix = oscillatorA, oscillatorB, suOscillator :> _ ;

filterA = filter(cutoffA, resonanceA, modeA, 1.0);
filterB = filter(cutoffB, resonanceB, modeB, 1.0);

singleFilter = oscillatorsMix <: _,_,_ : filterA;
routingSerial = oscillatorsMix <: _,_,_ : filterA <: _,_,_ : filterB;
routingParallel = (oscillatorsMix <: _,_,_ : filterA), (oscillatorsMix <: _,_,_ : filterB) :> _;

signal = singleFilter, routingSerial, routingParallel : ba.selectn(3, filterRouting);

process = signal * env1;
