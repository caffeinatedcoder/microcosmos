/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __FASE_LUNARE_CONSTANTS__
#define __FASE_LUNARE_CONSTANTS__

#include <Arduino.h>

typedef void (*voidFuncPtr)(void);

#define MICROCOSMOS
//#define DEBUG_AUDIO_RESOURCES

// VEGA SPECIFIC
#define BUTTONS_LEDS 4
#define MODE_LEDS 4
#define IO_LEDS 4
#define BUTTONS 4
#define GATES 5
#define POTS 3
#define ENCODERS 3

// GENERAL
#define FPS 1000/15 ///30 //ms 30fps
#define ONE_SECOND 60000

// SD CARD
#define SDCARD_CS_PIN    BUILTIN_SDCARD
#define SDCARD_MOSI_PIN  11
#define SDCARD_SCK_PIN   13

// ANALOG POTS
#define EMA_ALPHA 0.2
#define MIN_ANALOG_VALUE 0
#define MAX_ANALOG_VALUE 1024

// ENCODER SPECIFIC OK
#define ENCODER_A_A_PIN 2
#define ENCODER_A_B_PIN 3
#define ENCODER_B_A_PIN 4
#define ENCODER_B_B_PIN 5
#define ENCODER_C_A_PIN 24
#define ENCODER_C_B_PIN 25

#define ENCODER_A_BUTTON_PIN 7
#define ENCODER_B_BUTTON_PIN 8
#define ENCODER_C_BUTTON_PIN 28

// BUTTONS OK
#define BUTTON_A_PIN 39
#define BUTTON_B_PIN 34
#define BUTTON_C_PIN 33
#define BUTTON_D_PIN 32

// POTS
#define ANALOG_POT_PIN_A 64 //A10
#define ANALOG_POT_PIN_B 15
#define ANALOG_POT_PIN_C 14

// LEDS OK
#define LED_BTN_A_PIN 38
#define LED_BTN_B_PIN 37
#define LED_BTN_C_PIN 36
#define LED_BTN_D_PIN 35

#define LED_MODE_A_PIN 6
#define LED_MODE_B_PIN 9
#define LED_MODE_C_PIN 29
#define LED_MODE_D_PIN 30

#define LED_IO_A_PIN 20
#define LED_IO_B_PIN 23
#define LED_IO_C_PIN 22
#define LED_IO_D_PIN 21

// OLED SPECIFIC
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET_PIN -1

//  AUDIO OUTPUT
// #define AUDIOOUT1   A21
//  AUDIO IN VIA RANDOM INPUT
// #define AUDIOCRTL1  16

//  AUDIO OUTPUT
// #define AUDIOOUT1   A21
//  AUDIO IN VIA RANDOM INPUT
// #define AUDIOCRTL1  16
//  ON PCB SD CARD SLOT
// #define SDCS1   10
// #define SDIN    12
// #define SDOUT   11
// #define SCK0    13

#endif
