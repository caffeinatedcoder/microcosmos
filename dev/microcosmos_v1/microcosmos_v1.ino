/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "src/engine.h"
#include "src/programs/_include.h"
#include "src/utils/_include.h"

#include "USBHost_t36.h"

Engine *engine = Engine::Instance();

void setup() {
  Serial.begin(9600);

  engine->Setup();
  engine->programSwitcher->PushProgram(new ProgramStartup());
  engine->programSwitcher->PushProgram(new MicrocosmosFaustProgram());
}

void loop() {
  engine->Loop();
}
