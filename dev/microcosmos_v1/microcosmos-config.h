/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __MICROCOSMOS_CONFIG__
#define __MICROCOSMOS_CONFIG__

#define ENGINE "microcosmos"
#define DSP "syxtyofjune"
#define VERSION "0.1.219"

struct MenuParam {
  uint8_t address;
  uint8_t page;
  const char * param;
  const char * display;
  uint8_t value;
  byte controller;
  bool locked;
};

// define the number of pages
#define MENU_PAGES 5
// define how many params are shown on a page
#define PARAMS_PER_PAGE 4
#define MENU_PARAMS MENU_PAGES * PARAMS_PER_PAGE

// define the page titles
const char* const pageTitles[MENU_PAGES] = {
  "OSC",
  "VCF",
  "ENV",
  "ENV MOD",
  "LFO MOD"
};

// define the menu params
const MenuParam menuParams[MENU_PARAMS] = {
  // address, page, faust param, displayed param, default value, CC, locked (always true)
  // if faust param is NULL it isn't shown on the display
  // OSC
  {0, 0, "duty", "SQUARE PWM", 90, 21, true},
  {1, 0, "vol_a", "SQUARE VOLUME", 64, 22, true},
  {2, 0, "vol_b", "SAW VOLUME", 0, 23, true},
  {3, 0, "vol_c", "SUB VOLUME", 73, 24, true},
  // VCF
  {4, 1, "cutoff", "CUTOFF", 64, 25, true},
  {5, 1, "resonance", "RESONANCE", 0, 26, true},
  {6, 1, NULL, "", 0, 0, true},
  {7, 1, NULL, "", 0, 0, true},
  // ENV
  {8, 2, "1a", "ATTACK", 0, 41, true},
  {9, 2, "2d", "DECAY", 68, 42, true},
  {10, 2, "3s", "SUSTAIN", 28, 43, true},
  {11, 2, "4r", "RELEASE", 21, 44, true},
  // ENV MOD
  {12, 3, "pitch_env", "PITCH ENV", 0, 45, true},
  {13, 3, "cutoff_env", "VCF ENV", 65, 46, true},
  {14, 3, "duty_env", "PWM ENV", 0, 47, true},
  {15, 3, NULL, "", 0, 0, true},
  // LFO MOD
  {16, 4, "lfo_1_freq", "LFO RATE", 16, 48, true},
  {17, 4, "pitch_lfo", "PITCH LFO", 0, 27, true},
  {18, 4, "cutoff_lfo", "VCF LFO", 0, 28, true},
  {19, 4, "duty_lfo", "PWM LFO", 54, 0, true}
};

#endif
