/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "button.h"

Button::Button() {
}

void Button::Setup(uint8_t pin, voidFuncPtr interruptCallback) {
  _pin = pin;
  pinMode(_pin, INPUT_PULLUP);
  _lastClick = millis();
  _pressed = false;

  if (interruptCallback) {
    attachInterrupt(digitalPinToInterrupt(_pin), interruptCallback, CHANGE);
  }
}

uint8_t Button::Dispatch() {
  bool pressed = digitalRead(_pin) == LOW;

  if (pressed == _pressed) {
    return BUTTON_IDLE;
  } else {
    unsigned long current = millis();
    unsigned long elapsed = current - _lastClick;

    if (elapsed < 150) {
      _pressed = false;
      return BUTTON_IDLE;
    }

    _lastClick = current;
    _pressed = pressed;
    return _pressed ? BUTTON_PRESSED : BUTTON_RELEASED;
  }
}
