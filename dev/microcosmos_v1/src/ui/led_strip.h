/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __UI_LED_STRIP__
#define __UI_LED_STRIP__

#include "../../defines.h"
#if defined(SUB_ORBITAL)

#include <Adafruit_NeoPixel.h>

inline Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

class LedStrip {
public:
  void Setup();
  void SwitchOnMatrixLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffMatrixLed(uint8_t ledNum);
  void SwitchOnButtonLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffButtonLed(uint8_t ledNum);
  void SwitchOnKnobLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffKnobLed(uint8_t ledNum);
  void SwitchOnMatrixRow(uint8_t rowNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffMatrixRow(uint8_t rowNum);
  void SwitchOnMatrixColumn(uint8_t columnNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffMatrixColumn(uint8_t columnNum);
  void SwitchOnIoLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffIoLed(uint8_t ledNum);
  void SwitchOffAll();
  void Show();

private:
  Adafruit_NeoPixel *_strip;
  uint8_t knobLedPositions[3] = {0, 1, 2};
  uint8_t buttonLedPositions[8] = {9, 7, 5, 3, 10, 8, 6, 4};
  uint8_t matrixLedPositions[16] = {
    11, 15, 19, 23,
    12, 16, 20, 24,
    13, 17, 21, 25,
    14, 18, 22, 26
  };
  uint8_t ioLedPositions[9] = {27, 28, 29, 30, 31, 32, 33, 34, 35};
  void setLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
};

#endif
#endif
