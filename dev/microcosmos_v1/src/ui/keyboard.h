/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __UI_KEYBOARD__
#define __UI_KEYBOARD__

#include "../../defines.h"
#if defined(SUB_ORBITAL)

#include <Wire.h>
#include <Arduino.h>

class Keyboard {
public:
  Keyboard();
  void Setup(uint8_t defaultKeyboardMode, voidFuncPtr interruptCallback);
  void Scan();
  void InterruptCallback();
  uint8_t GetKeyDown();
  uint8_t GetKeyUp();
  int GetCalls();
private:
  const int *_buttonMap;
  int _address;
  uint8_t _keyDown;
  uint8_t _keyUp;
  int _calls;
  bool _keyboardChanged;

  void clearFlag();
  uint8_t getState();
};

#endif
#endif
