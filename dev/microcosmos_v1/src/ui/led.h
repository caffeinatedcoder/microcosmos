/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __FASE_LUNARE_CORE_LED__
#define __FASE_LUNARE_CORE_LED__

#include <Arduino.h>

class Led {
public:
  Led();
  void Setup(int pin, bool isDigital = false);
  void Reset();
  void Set(bool value);
  void Set(int value);
  void Show();
private:
  int _pin;
  int _analogValue;
  bool _isDigital;
};

#endif
