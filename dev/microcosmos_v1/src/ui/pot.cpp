/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "pot.h"

Pot::Pot() {

}

void Pot::Setup(uint8_t pin, bool inverted) {
  _pin = pin;
  _ema = 0;
  _max = 0;
  _inverted = inverted;

  _ema = analogRead(_pin);
  _delta = 0;

  if (_inverted) {
    _ema = _maxRef - _ema;
  }

  _max = _ema;
  _lastRead = _ema;
}

bool Pot::Changed() {
  ReadAnalog();
  if (_lastRead != _ema) {
    _lastRead = _ema;
    return true;
  }
  return false;
}

int Pot::GetLastRead() {
  return _lastRead;
}

int Pot::ReadRaw() {
  int value = analogRead(_pin);
  if (_inverted) {
    value = _maxRef - value;
  }
  _max = value > _max ? value : _max;
  return value;
}

int Pot::GetMax() {
  return _max;
}

int Pot::ReadAnalog() {
  int value = analogRead(_pin);
  if (_inverted) {
    value = _maxRef - value;
  }

  _max = value > _max ? value : _max;
  _ema = exponential_moving_average(value, _ema);
  return _ema;
}

int Pot::ReadAsUnipolar(int value) {
  return map(value, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE, 0, 127);
}

int Pot::ReadAsBipolar(int value) {
  return map(value, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE, -64, 64);
}

int Pot::ReadAs128(int value) {
  return ReadAsUnipolar(value);
}

int Pot::ReadAs64(int value) {
  return map(value, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE, 0, 64);
}

int Pot::ReadAs16(int value) {
  return map(value, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE, 0, 16);
}

int Pot::ReadAs8(int value) {
  return map(value, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE, 0, 8);
}

int Pot::ReadAs4(int value) {
  return map(value, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE, 0, 4);
}
