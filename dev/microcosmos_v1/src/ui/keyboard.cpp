/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "keyboard.h"
#if defined(SUB_ORBITAL)

Keyboard::Keyboard() {
}

void Keyboard::Setup(uint8_t defaultKeyboardMode, voidFuncPtr interruptCallback) {
  Wire.begin();
  for(uint8_t i = 0; i < 10; i+=2) {
    Wire.beginTransmission(KEYBOARD_ADDRESS);
    Wire.write(KEYBOARD_CONFIG_COMMANDS[i]);
    Wire.write(KEYBOARD_CONFIG_COMMANDS[i+1]);
    Wire.endTransmission();
  }

  pinMode(KEYBOARD_INT_PIN, INPUT);
  pinMode(KEYBOARD_RST_PIN, OUTPUT);
  digitalWrite(KEYBOARD_RST_PIN, HIGH);
  attachInterrupt(digitalPinToInterrupt(KEYBOARD_INT_PIN), interruptCallback, FALLING);

  _calls = 0;
  _keyboardChanged = false;
  _keyDown = -1;
  _keyUp = -1;
}

void Keyboard::Scan() {
  if(_keyboardChanged) {
    _calls ++;
    uint8_t key_code = getState();

    int index = -1;

    if(key_code & KEYBOARD_KEY_DOWN) {
      for (int i = 0; i < 9; i++) {
        if (KEYBOARD_BUTTON_MAP[i] == (key_code & KEYBOARD_KEY_MASK)) {
          index = i;
          break;
        }
      }
      _keyDown = index;
      _keyUp = -1;
    } else {
      for (int i = 0; i < 9; i++) {
        if (KEYBOARD_BUTTON_MAP[i] == (key_code & KEYBOARD_KEY_MASK)) {
          index = i;
          break;
        }
      }
      _keyDown = -1;
      _keyUp = index;
    }

    _keyboardChanged = false;
    clearFlag();
  }
}

void Keyboard::InterruptCallback() {
  _keyboardChanged = true;
}

uint8_t Keyboard::GetKeyDown() {
  return _keyDown;
}

uint8_t Keyboard::GetKeyUp() {
  return _keyUp;
}

int Keyboard::GetCalls() {
  return _calls;
}

void Keyboard::clearFlag() {
  Wire.beginTransmission(KEYBOARD_ADDRESS);
  Wire.write(KEYBOARD_FLAG_REGISTER);
  Wire.write(0x0F);
  Wire.endTransmission();
}

uint8_t Keyboard::getState() {
  uint8_t key;

  Wire.beginTransmission(KEYBOARD_ADDRESS);
  Wire.write(KEYBOARD_STATE_REGISTER);
  Wire.endTransmission();

  Wire.requestFrom(KEYBOARD_ADDRESS, 1);  // request 1 bytes from slave device
  while (Wire.available()) {              // slave may send less than requested
    key = Wire.read();                    // receive a byte as character
  }
  return key;
}

#endif
