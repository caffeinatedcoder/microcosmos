/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __FASE_LUNARE_CORE_POT__
#define __FASE_LUNARE_CORE_POT__

#include <Arduino.h>
#include "../../defines.h"
#include "../utils/_include.h"

class Pot {
public:
  Pot();
  void Setup(uint8_t pin, bool inverted);
  bool Changed();
  int GetLastRead();
  int ReadAnalog();
  int ReadRaw();
  static int ReadAsUnipolar(int value);
  static int ReadAsBipolar(int value);
  static int ReadAs128(int value);
  static int ReadAs64(int value);
  static int ReadAs16(int value);
  static int ReadAs8(int value);
  static int ReadAs4(int value);
  int GetMax();
private:
  uint8_t _pin;
  int _ema;
  int _delta;
  int _max;
  int _minRef;
  int _maxRef;
  bool _inverted;
  int _lastRead;
};

#endif
