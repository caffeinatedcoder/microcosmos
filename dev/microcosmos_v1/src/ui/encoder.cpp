/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "encoder.h"

Encoder::Encoder() {
}

void Encoder::Setup(int pin1, int pin2, voidFuncPtr interruptCallback) {
	_pin1 = pin1;
	_pin2 = pin2;

	_value = 0;

	_lastClick = millis();

	pinMode(_pin1, INPUT_PULLUP);
	pinMode(_pin2, INPUT_PULLUP);

	attachInterrupt(digitalPinToInterrupt(_pin1), interruptCallback, CHANGE);
}

bool Encoder::InterruptCallback() {
	if (digitalRead(_pin2) == LOW) {
		return false;
	}

	unsigned long current = millis();
	unsigned long elapsed = current - _lastClick;
	_lastClick = current;

	int increase = 1;

	if (elapsed < 25) {
		increase = 8;
	}

	if (digitalRead(_pin1) == digitalRead(_pin2)) {
		_value += increase;
  } else {
		_value -= increase;
  }

	return true;
}

void Encoder::Reset(int value) {
	_value = value;
}

int Encoder::GetValue() {
	return _value;
}
