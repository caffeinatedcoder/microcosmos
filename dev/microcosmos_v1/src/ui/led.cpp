/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "led.h"

Led::Led() {
}

void Led::Setup(int pin, bool isDigital) {
	_pin = pin;
	_analogValue = 0;
	_isDigital = isDigital;
	pinMode(_pin, OUTPUT);
}

void Led::Reset() {
  _analogValue = 0;
}

void Led::Set(bool value) {
	_analogValue = value ? 255 : 0;
}

void Led::Set(int value) {
  _analogValue = value <= 255 ? value : 255;
}

void Led::Show() {
	if (_isDigital) {
		digitalWrite(_pin, _analogValue > 0 ? HIGH : LOW);
	} else {
		analogWrite(_pin, _analogValue);
	}

}
