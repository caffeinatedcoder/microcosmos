/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __UI_BUTTON__
#define __UI_BUTTON__

#include "../../defines.h"
#include "../utils/_include.h"
#include <Arduino.h>

#define BUTTON_IDLE 0
#define BUTTON_PRESSED 1
#define BUTTON_RELEASED 2

/*
PLEASE READ:
https://create.arduino.cc/projecthub/Svizel_pritula/10-buttons-using-1-interrupt-2bd1f8

http://www.gammon.com.au/interrupts

https://my.eng.utah.edu/~cs5780/debouncing.pdf
*/

class Button {
public:
  Button();
  void Setup(uint8_t pin, voidFuncPtr interruptCallback = NULL);
  uint8_t Dispatch();
private:
  uint8_t _pin;
  unsigned long _lastClick;
  bool _pressed;
};

#endif
