/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __SUB_ORBITAL_ENGINE__
#define __SUB_ORBITAL_ENGINE__

#include "../defines.h"
#ifdef MICROCOSMOS

#include <Audio.h>
#include <Arduino.h>
#include <Bounce2.h>

#include "./core/_include.h"
#include "./io/_include.h"
#include "./ui/_include.h"
#include "./utils/_include.h"

#include "USBHost_t36.h"

#define TYPE_BUTTON 0
#define TYPE_ENCODER 1

class Engine {
public:
  //singleton pattern
  static Engine *Instance();
  //end singleton pattern

  void Setup();
  void Loop();

  ProgramSwitcher *programSwitcher;

  // board inputs
  Button *buttons[BUTTONS];
  Button *encoderButtons[ENCODERS];
  Pot *pots[POTS];
  Encoder *encoders[ENCODERS];
  // board display
  Oled *oled;
  Led *buttonLeds[BUTTONS_LEDS];
  Led *modeLeds[MODE_LEDS];
  Led *ioLeds[IO_LEDS];

  Sdrw *sdrw;

private:
  // singleton pattern
  static Engine* _instance;
  Engine();
  ~Engine();
  Engine(const Engine&);
  Engine& operator=(const Engine&);
  // end singleton pattern

  void encoderUpdate(int index);
  void buttonUpdate(int index, int type);

  static void encoderAInterruptCallback();
  static void encoderBInterruptCallback();
  static void encoderCInterruptCallback();

  static void buttonAInterruptCallback();
  static void buttonBInterruptCallback();
  static void buttonCInterruptCallback();
  static void buttonDInterruptCallback();

  static void encoderButtonAInterruptCallback();
  static void encoderButtonBInterruptCallback();
  static void encoderButtonCInterruptCallback();

  static void onNoteOn(byte channel, byte note, byte velocity);
  static void onNoteOff(byte channel, byte note, byte velocity);
  static void onControlChange(byte channel, byte control, byte value);

  bool _state;
  Timer *_mainTimer;
};

#endif
#endif
