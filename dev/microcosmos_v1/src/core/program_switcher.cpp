/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "program_switcher.h"

ProgramSwitcher::ProgramSwitcher() {
  _current = 0;
  _root = NULL;
}

void ProgramSwitcher::PushProgram(Program *program) {
  if (_root == NULL) {
    _root = program;
    return;
  }

  Program *current = _root;
  while (current->GetNext() != NULL) {
      current = current->GetNext();
  }
  current->SetNext(program);
}

void ProgramSwitcher::NextProgram() {
  _current = (_current + 1) % CountPrograms();
  GetCurrentProgram()->Reset();
}

void ProgramSwitcher::PrevProgram() {
  _current = (_current - 1) % CountPrograms();
  GetCurrentProgram()->Reset();
}

void ProgramSwitcher::GoToProgram(int index) {
  _current = index % CountPrograms();
  GetCurrentProgram()->Reset();
}

Program *ProgramSwitcher::GetCurrentProgram() {
  return getNth(_current);
}

int ProgramSwitcher::CountPrograms() {
  int count = 0;
  Program *current = _root;
  while (current != NULL) {
    current = current->GetNext();
    count++;
  }
  return count;
}

Program *ProgramSwitcher::getNth(int index) {
  int count = 0;
  Program *found = NULL;
  Program *current = _root;
  while(current != NULL) {
    if (count == index) {
      found = current;
      break;
    }
    count++;
    current = current->GetNext();
  }

  return found;
}

int ProgramSwitcher::CurrentProgramIndex() {
  return _current;
}
