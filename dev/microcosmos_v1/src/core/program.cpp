/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "program.h"

Program::Program() {
  _next = NULL;
}

void Program::Setup() {
}

void Program::Run() {
}

void Program::Loop() {
}

void Program::Reset() {
}

void Program::OnKeyDown(int key) {
}

void Program::OnKeyUp(int key) {
}

void Program::OnPotChange(int index, int value) {
}

void Program::OnEncoderChange(int index, int value) {
}

void Program::OnGateIn(uint8_t channel) {
}

void Program::OnNoteOn(byte channel, byte note, byte velocity) {
}

void Program::OnNoteOff(byte channel, byte note, byte velocity) {
}

void Program::OnControlChange(byte channel, byte control, byte value) {
}

void Program::SetNext(Program *program) {
    _next = program;
}

Program *Program::GetNext() {
  return _next;
}
