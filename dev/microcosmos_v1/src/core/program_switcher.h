/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __CORE_PROGRAM_SWITCHER__
#define __CORE_PROGRAM_SWITCHER__

#include <Arduino.h>
#include "program.h"

class ProgramSwitcher {
public:
  ProgramSwitcher();
  void PushProgram(Program *program);
  void NextProgram();
  void PrevProgram();
  void GoToProgram(int index);
  Program *GetCurrentProgram();
  int CountPrograms();
  int CurrentProgramIndex();
private:
  Program *getNth(int index);

  Program *_root;
  int _current;
};

#endif
