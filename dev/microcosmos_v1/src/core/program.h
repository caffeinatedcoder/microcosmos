/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __CORE_PROGRAM__
#define __CORE_PROGRAM__

#include <Arduino.h>

class Program {
public:
  Program();
  virtual void Setup();
  virtual void Run();
  virtual void Loop();
  virtual void Reset();
  virtual void OnKeyDown(int key);
  virtual void OnKeyUp(int key);
  virtual void OnPotChange(int index, int value);
  virtual void OnEncoderChange(int index, int value);
  virtual void OnGateIn(uint8_t channel);
  virtual void OnNoteOn(byte channel, byte note, byte velocity);
  virtual void OnNoteOff(byte channel, byte note, byte velocity);
  virtual void OnControlChange(byte channel, byte control, byte value);
  void SetNext(Program *program);
  Program *GetNext();
private:
  Program *_next;
};

#endif
