/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#ifndef __SYNTH_AD__
#define __SYNTH_AD__
#include "Arduino.h"
#include "AudioStream.h"
#include <Audio.h>
#include "utility/dspinst.h"

#define SAMPLES_PER_MSEC (AUDIO_SAMPLE_RATE_EXACT/1000.0)

class Ad : public AudioStream
{
public:
	Ad() : AudioStream(1, inputQueueArray) {
		state = 0;
		attack(10.5f);
		release(300.0f);
		releaseNoteOn(5.0f);
	}
	void noteOn();
	//void noteOff();
	void attack(float milliseconds) {
		attack_count = milliseconds2count(milliseconds);
		if (attack_count == 0) attack_count = 1;
	}
	void release(float milliseconds) {
		release_count = milliseconds2count(milliseconds);
		if (release_count == 0) release_count = 1;
	}
	void releaseNoteOn(float milliseconds) {
		release_forced_count = milliseconds2count(milliseconds);
		if (release_count == 0) release_count = 1;
	}
	bool isActive();
	using AudioStream::release;
	virtual void update(void);
private:
	uint16_t milliseconds2count(float milliseconds) {
		if (milliseconds < 0.0) milliseconds = 0.0;
		uint32_t c = ((uint32_t)(milliseconds*SAMPLES_PER_MSEC)+7)>>3;
		if (c > 65535) c = 65535; // allow up to 11.88 seconds
		return c;
	}
	audio_block_t *inputQueueArray[1];
	// state
	uint8_t  state;      // idle, delay, attack, hold, decay, sustain, release, forced
	uint16_t count;      // how much time remains in this state, in 8 sample units
	int32_t  mult_hires; // attenuation, 0=off, 0x40000000=unity gain
	int32_t  inc_hires;  // amount to change mult_hires every 8 samples

	// settings
	uint16_t attack_count;
	uint16_t release_count;
	uint16_t release_forced_count;

};

#undef SAMPLES_PER_MSEC
#endif
