/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "ad.h"

#define STATE_IDLE 0
#define STATE_ATTACK 1
#define STATE_RELEASE	2
#define STATE_FORCED 3

void Ad::noteOn(void)
{
	__disable_irq();
	if (state == STATE_IDLE || release_forced_count == 0) {
		mult_hires = 0;
    state = STATE_ATTACK;
    count = attack_count;
    inc_hires = 0x40000000 / (int32_t)count;
	} else if (state != STATE_FORCED) {
		state = STATE_FORCED;
		count = release_forced_count;
		inc_hires = (-mult_hires) / (int32_t)count;
	}
	__enable_irq();
}

/*void Ad::noteOff(void)
{
	__disable_irq();
	if (state != STATE_IDLE && state != STATE_FORCED) {
		state = STATE_RELEASE;
		count = release_count;
		inc_hires = (-mult_hires) / (int32_t)count;
	}
	__enable_irq();
}*/

void Ad::update(void)
{
	audio_block_t *block;
	uint32_t *p, *end;
	uint32_t sample12, sample34, sample56, sample78, tmp1, tmp2;

	block = receiveWritable();
	if (!block) return;
	if (state == STATE_IDLE) {
		release(block);
		return;
	}
	p = (uint32_t *)(block->data);
	end = p + AUDIO_BLOCK_SAMPLES/2;

	while (p < end) {
		// we only care about the state when completing a region
		if (count == 0) {
			if (state == STATE_ATTACK) {
        state = STATE_RELEASE;
        count = release_count;
        inc_hires = (-mult_hires) / (int32_t)count;
				continue;
			} else if (state == STATE_RELEASE) {
				state = STATE_IDLE;
				while (p < end) {
					*p++ = 0;
					*p++ = 0;
					*p++ = 0;
					*p++ = 0;
				}
				break;
			} else if (state == STATE_FORCED) {
				mult_hires = 0;
				state = STATE_ATTACK;
				count = attack_count;
				inc_hires = 0x40000000 / (int32_t)count;
			}
		}

		int32_t mult = mult_hires >> 14;
		int32_t inc = inc_hires >> 17;
		// process 8 samples, using only mult and inc (16 bit resolution)
		sample12 = *p++;
		sample34 = *p++;
		sample56 = *p++;
		sample78 = *p++;
		p -= 4;
		mult += inc;
		tmp1 = signed_multiply_32x16b(mult, sample12);
		mult += inc;
		tmp2 = signed_multiply_32x16t(mult, sample12);
		sample12 = pack_16b_16b(tmp2, tmp1);
		mult += inc;
		tmp1 = signed_multiply_32x16b(mult, sample34);
		mult += inc;
		tmp2 = signed_multiply_32x16t(mult, sample34);
		sample34 = pack_16b_16b(tmp2, tmp1);
		mult += inc;
		tmp1 = signed_multiply_32x16b(mult, sample56);
		mult += inc;
		tmp2 = signed_multiply_32x16t(mult, sample56);
		sample56 = pack_16b_16b(tmp2, tmp1);
		mult += inc;
		tmp1 = signed_multiply_32x16b(mult, sample78);
		mult += inc;
		tmp2 = signed_multiply_32x16t(mult, sample78);
		sample78 = pack_16b_16b(tmp2, tmp1);
		*p++ = sample12;
		*p++ = sample34;
		*p++ = sample56;
		*p++ = sample78;
		// adjust the long-term gain using 30 bit resolution (fix #102)
		// https://github.com/PaulStoffregen/Audio/issues/102
		mult_hires += inc_hires;
		count--;
	}
	transmit(block);
	release(block);
}

bool Ad::isActive()
{
	uint8_t current_state = *(volatile uint8_t *)&state;
	if (current_state == STATE_IDLE) return false;
	return true;
}
