/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "engine.h"

USBHost myusb;
USBHub hub1(myusb);
USBHub hub2(myusb);
USBHub hub3(myusb);
MIDIDevice midi1(myusb);

Engine* Engine::_instance = NULL;

Engine* Engine::Instance() {
	if(_instance == NULL){
		_instance = new Engine();
	}
	return _instance;
}

Engine::~Engine() {
	if(_instance != 0) delete _instance;
}

Engine::Engine() {
	_mainTimer = new Timer();
	programSwitcher = new ProgramSwitcher();

	for (int i=0; i<BUTTONS; i++) {
		buttons[i] = new Button();
	}

	for (int i=0; i<POTS; i++) {
		pots[i] = new Pot();
	}

	for (int i=0; i<ENCODERS; i++) {
		encoders[i] = new Encoder();
		encoderButtons[i] = new Button();
	}

	oled = new Oled();

	for (int i=0; i<BUTTONS_LEDS; i++) {
		buttonLeds[i] = new Led();
	}

	for (int i=0; i<MODE_LEDS; i++) {
		modeLeds[i] = new Led();
	}

	for (int i=0; i<IO_LEDS; i++) {
		ioLeds[i] = new Led();
	}
}

void Engine::Setup() {
	myusb.begin();
  midi1.setHandleNoteOff(onNoteOff);
	midi1.setHandleNoteOn(onNoteOn);
	midi1.setHandleControlChange(onControlChange);

	_mainTimer->Setup();

	buttons[0]->Setup(BUTTON_A_PIN, buttonAInterruptCallback);
	buttons[1]->Setup(BUTTON_B_PIN, buttonBInterruptCallback);
	buttons[2]->Setup(BUTTON_C_PIN, buttonCInterruptCallback);
	buttons[3]->Setup(BUTTON_D_PIN, buttonDInterruptCallback);

	pots[0]->Setup(ANALOG_POT_PIN_A, false);
	pots[1]->Setup(ANALOG_POT_PIN_B, false);
	pots[2]->Setup(ANALOG_POT_PIN_C, false);

	encoders[0]->Setup(ENCODER_A_B_PIN, ENCODER_A_A_PIN, encoderAInterruptCallback);
	encoders[1]->Setup(ENCODER_B_B_PIN, ENCODER_B_A_PIN, encoderBInterruptCallback);
	encoders[2]->Setup(ENCODER_C_B_PIN, ENCODER_C_A_PIN, encoderCInterruptCallback);

	encoderButtons[0]->Setup(ENCODER_A_BUTTON_PIN, encoderButtonAInterruptCallback);
	encoderButtons[1]->Setup(ENCODER_B_BUTTON_PIN, encoderButtonBInterruptCallback);
	encoderButtons[2]->Setup(ENCODER_C_BUTTON_PIN, encoderButtonCInterruptCallback);

	oled->Setup();

	buttonLeds[0]->Setup(LED_BTN_A_PIN, false);
	buttonLeds[1]->Setup(LED_BTN_B_PIN, false);
	buttonLeds[2]->Setup(LED_BTN_C_PIN, false);
	buttonLeds[3]->Setup(LED_BTN_D_PIN, false);

	modeLeds[0]->Setup(LED_MODE_A_PIN, false);
	modeLeds[1]->Setup(LED_MODE_B_PIN, false);
	modeLeds[2]->Setup(LED_MODE_C_PIN, false);
	modeLeds[3]->Setup(LED_MODE_D_PIN, false);

	ioLeds[0]->Setup(LED_IO_A_PIN, false);
	ioLeds[1]->Setup(LED_IO_B_PIN, false);
	ioLeds[2]->Setup(LED_IO_C_PIN, false);
	ioLeds[3]->Setup(LED_IO_D_PIN, false);

	// test sd
	/*
	sdrw = new Sdrw();
	std::vector<char *> list = sdrw->List("/");

	Serial.print("files on card: ");
	Serial.println(list.size());
	for (uint8_t i=0; i < list.size(); i++) {
		Serial.print(i);
		Serial.print(". ");
		Serial.println(list.at(i));
	}
	Serial.println("engine setup ok");
	*/
}

void Engine::Loop() {
	midi1.read();

	// execute program not constrained to FPS
	if (programSwitcher->GetCurrentProgram() != NULL) {
		programSwitcher->GetCurrentProgram()->Run();
	}

  unsigned long elapsed = _mainTimer->GetElapsed();

  if (elapsed >= FPS) {
    _mainTimer->Update();

		oled->ClearDisplay();

		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Reset();
		}

		for (int i=0; i<MODE_LEDS; i++) {
			modeLeds[i]->Reset();
		}

		for (int i=0; i<IO_LEDS; i++) {
			ioLeds[i]->Reset();
		}

		// execute program
		if (programSwitcher->GetCurrentProgram() != NULL) {
			// dispatch board changes
			for (int i=0; i<POTS; i++) {
				if (pots[i]->Changed()) {
					programSwitcher->GetCurrentProgram()->OnPotChange(i, Pot::ReadAs128(pots[i]->GetLastRead()));
				}
			}

			programSwitcher->GetCurrentProgram()->Loop();
		}

		oled->Render();

		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Show();
		}

		for (int i=0; i<MODE_LEDS; i++) {
			modeLeds[i]->Show();
		}

		for (int i=0; i<IO_LEDS; i++) {
			ioLeds[i]->Show();
		}
  }
}

void Engine::encoderUpdate(int index) {
	Encoder* e = Engine::Instance()->encoders[index];
	bool changed = e->InterruptCallback();

	if (programSwitcher->GetCurrentProgram() != NULL) {
		if (changed) {
			int value = e->GetValue();
			programSwitcher->GetCurrentProgram()->OnEncoderChange(index, value);
		}
	}
}

void Engine::buttonUpdate(int index, int type) {
	uint8_t value = (type == TYPE_BUTTON) ? buttons[index]->Dispatch() : encoderButtons[index]->Dispatch();

	uint8_t updatedIndex = (type == TYPE_BUTTON) ? index : index + 4;

	if (programSwitcher->GetCurrentProgram() != NULL) {
		if (value == BUTTON_PRESSED) {
			programSwitcher->GetCurrentProgram()->OnKeyDown(updatedIndex);
		} else if (value == BUTTON_RELEASED) {
			programSwitcher->GetCurrentProgram()->OnKeyUp(updatedIndex);
		}
	}
}

void Engine::encoderAInterruptCallback() {
	Engine::Instance()->encoderUpdate(0);
}

void Engine::encoderBInterruptCallback() {
	Engine::Instance()->encoderUpdate(1);
}

void Engine::encoderCInterruptCallback() {
	Engine::Instance()->encoderUpdate(2);
}

void Engine::buttonAInterruptCallback() {
	Engine::Instance()->buttonUpdate(0, TYPE_BUTTON);
}

void Engine::buttonBInterruptCallback() {
	Engine::Instance()->buttonUpdate(1, TYPE_BUTTON);
}

void Engine::buttonCInterruptCallback() {
	Engine::Instance()->buttonUpdate(2, TYPE_BUTTON);
}

void Engine::buttonDInterruptCallback() {
	Engine::Instance()->buttonUpdate(3, TYPE_BUTTON);
}

void Engine::encoderButtonAInterruptCallback() {
	Engine::Instance()->buttonUpdate(0, TYPE_ENCODER);
}

void Engine::encoderButtonBInterruptCallback() {
	Engine::Instance()->buttonUpdate(1, TYPE_ENCODER);
}

void Engine::encoderButtonCInterruptCallback() {
	Engine::Instance()->buttonUpdate(2, TYPE_ENCODER);
}

void Engine::onNoteOn(byte channel, byte note, byte velocity) {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnNoteOn(channel, note, velocity);
}

void Engine::onNoteOff(byte channel, byte note, byte velocity) {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnNoteOff(channel, note, velocity);
}

void Engine::onControlChange(byte channel, byte control, byte value) {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnControlChange(channel, control, value);
}
