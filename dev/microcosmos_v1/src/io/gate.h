/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __IO_GATE__
#define __IO_GATE__

#include "../../defines.h"
#include <Arduino.h>

class Gate {
public:
  Gate();
  void Setup(uint8_t pin, uint8_t mode, voidFuncPtr interruptCallback = NULL);
  void Out(int value);
  int calls;
private:
  uint8_t _pin;
  uint8_t _mode;
  int _value;
};

#endif
