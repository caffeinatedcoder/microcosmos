/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "sdrw.h"

Sdrw::Sdrw() {
  if (!SD.begin(SDCARD_CS_PIN)) {
    Serial.println("SD initialization failed!");
  }
}

std::vector<char*> Sdrw::List(const char *dirName) {
  std::vector<char*> list;

  File dir = SD.open("/");

  if (!dir) {
    Serial.println("Directory not found");
    return list;
  }

  while (true) {
    File entry =  dir.openNextFile();
    if (!entry) {
      break;
    }

    if (!entry.isDirectory()) {
      char* s = entry.name();
      char* c = (char*)malloc(strlen(c)+1);
      strcpy(c,s);
      list.push_back(c);
    }
    entry.close();
  }

  return list;
}

const char *Sdrw::ReadFile(const char *fileName) {
  File file;
  unsigned long lSize;
  char * buffer;
  size_t result;

  file = SD.open(fileName, FILE_READ);
  if (!file) {
    return "";
  }

  // obtain file size:
  // fseek (file , 0 , SEEK_END);
  // lSize = ftell (file);
  // rewind (file);
  lSize = file.size();

  // allocate memory to contain the whole file:
  buffer = (char*) malloc (sizeof(char)*lSize);
  if (buffer == NULL) {
    // printf("Memory error\n");
    return "";
  }

  // copy the file into the buffer:
  // result = fread (buffer,1,lSize,file);
  result = file.read(buffer, lSize);
  if (result != lSize) {
    // printf("Reading error\n");
    return "";
  }

  // release the resource
  file.close();
  return buffer;
}

std::vector<Param*> Sdrw::ReadFileParams(const char *fileName) {
  std::vector<Param*> params;

  const char * config = ReadFile(fileName);

  std::vector<char*> lines = tokenize((char*)config, (char*)"\n");
  for (uint i=0;i<lines.size();i++) {
    Param *param = processParam(lines.at(i));
    if (param->valid) {
      params.push_back(param);
    }
  }

  return params;
}

std::vector<char *> Sdrw::tokenize(char *chunk, char *delimiter) {
  std::vector<char *> tokens;

  char *token = strtok((char *)chunk, delimiter);
  while (token != NULL) {
    tokens.push_back(token);
    token = strtok(NULL, delimiter);
  }

  return tokens;
}

Param* Sdrw::processParam(char *token) {
  Param *param = new Param();
  param->valid = false;

  std::vector<char *> paramData = tokenize(token, (char *)"=");

  if (paramData.size()==2) {
    param->name = paramData[0];
    param->value = paramData[1];
    param->valid = true;

    if (param->name[0] == '#') {
      param->valid = false;
    }
  }

  return param;
}
