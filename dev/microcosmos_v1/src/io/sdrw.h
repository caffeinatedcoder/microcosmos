/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __SDRW__
#define __SDRW__

#include "../../defines.h"
#include <Arduino.h>
#include <SPI.h>
#include <SD.h>

#include <vector>

struct Param {
  char *name;
  char *value;
  bool valid;
};

class Sdrw {
public:
  Sdrw();
  std::vector<char*> List(const char *dirName);
  const char *ReadFile(const char *fileName);
  std::vector<Param*> ReadFileParams(const char *fileName);
private:
  std::vector<char *> tokenize(char *chunk, char *delimiter);
  Param* processParam(char *token);
};

#endif
