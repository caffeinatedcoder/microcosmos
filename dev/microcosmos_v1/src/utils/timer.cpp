/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "timer.h"

Timer::Timer() {

}

void Timer::Setup() {
  last = millis();
  current = last;
}

unsigned long Timer::GetElapsed() {
  current = millis();

  if (current < last) {
    last = current;
  }

  return current - last;
}

void Timer::Update() {
  last = current;
}


// TODO: from python
/*
music calculator by d. pagliero
conversion from and to: bpm, time and samples

units:
- time is in milliseconds
- rate is in KHz
*/

/*
def bpmToTime(bpm,numerator,denominator):
	division = (numerator*1.)/denominator
	return 60000/bpm/(1./4./division)

def bpmToSamples(bpm,numerator,denominator,rate):
	time = bpmToTime(bpm,numerator,denominator)
	return timeToSamples(time,rate)

def timeToBpm(time,numerator,denominator):
	division = (numerator*1.)/denominator
	return 60000/time/(1./4./division)

def timeToSamples(time,rate):
	return time*rate

def samplesToTime(samples,rate):
	return samples/rate

def samplesToBpm(samples,rate,numerator,denominator):
	time = samplesToTime(samples,rate)
	return timeToBpm(time,numerator,denominator)
*/
