/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef CORE_TIMER
#define CORE_TIMER

#include <Arduino.h>

class Timer {
public:
  Timer();
  void Setup();
  unsigned long GetElapsed();
  void Update();

  // TODO: from python
  /*
  bpmToTime(bpm,numerator,denominator);
  bpmToSamples(bpm,numerator,denominator,rate);
  timeToBpm(time,numerator,denominator);
  timeToSamples(time,rate);
  samplesToTime(samples,rate);
  samplesToBpm(samples,rate,numerator,denominator);
  */
private:
  unsigned long last;
  unsigned long current;
};

#endif
