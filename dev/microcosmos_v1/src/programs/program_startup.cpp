/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "program_startup.h"

ProgramStartup::ProgramStartup() {
  Setup();
}

void ProgramStartup::Setup() {
  _tic = 0;
}

void ProgramStartup::Loop() {
  Engine::Instance()->oled->RenderStartupScreen();

  /*for (int i=0; i<BUTTONS_LEDS; i++) {
    Engine::Instance()->buttonLeds[i]->Set(_tic);
  }

  for (int i=0; i<MODE_LEDS; i++) {
    Engine::Instance()->modeLeds[i]->Set(_tic);
  }

  for (int i=0; i<IO_LEDS; i++) {
    Engine::Instance()->ioLeds[i]->Set(_tic);
  }*/

  Engine::Instance()->modeLeds[_tic]->Set(5);

  _tic++;

  if (_tic == 4) {
    _tic = 0;
    Engine::Instance()->programSwitcher->NextProgram();
  }
}

void ProgramStartup::OnKeyDown(int key) {

}

void ProgramStartup::OnKeyUp(int key) {

}
