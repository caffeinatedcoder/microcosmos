/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#ifndef __MICROCOSMOS_FAUST_PROGRAM__
#define __MICROCOSMOS_FAUST_PROGRAM__

#include <Arduino.h>
#include <Audio.h>

#include "../engine.h"
#include "../utils/_include.h"
#include "../../microcosmos-config.h"

#include "../faust/MicrocosmosDsp/MicrocosmosDsp.h"

// #define MENU_PAGES 5
// #define PARAMS_PER_PAGE 4
// #define MENU_PARAMS MENU_PAGES * PARAMS_PER_PAGE

/*struct MenuParam {
  uint8_t address;
  uint8_t page;
  const char * param;
  const char * display;
  uint8_t value;
  byte controller;
  bool locked;
};*/

class MicrocosmosFaustProgram : public Program {
public:
  MicrocosmosFaustProgram();
  void Setup();
  void Loop();
  void Run();
  void OnGateIn(uint8_t channel);
  void OnKeyDown(int key);
  void OnKeyUp(int key);
  void OnPotChange(int index, int value);
  void OnEncoderChange(int index, int value);
  void OnNoteOn(byte channel, byte note, byte velocity);
  void OnNoteOff(byte channel, byte note, byte velocity);
  void OnControlChange(byte channel, byte control, byte value);
private:
  uint8_t _page;
  int _lastNote;
  int _noteOffset;

  /*const char *_pageTitles[MENU_PAGES] = {
    "OSC",
    "VCF",
    "ENV",
    "ENV MOD",
    "LFO MOD"
  };*/

  MenuParam _menuParams[MENU_PARAMS];

  void updateMenu();
  MenuParam* getPageParam(int num);
};

#endif
