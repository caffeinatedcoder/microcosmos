/* MICROCOSMOS - Firmware for the MICROCOSMOS Board
 * Copyright (C) 2020 Faselunare
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
 
#include "program_microcosmos_faust.h"

MicrocosmosDsp voice;
AudioOutputAnalogStereo  dacs;
AudioConnection          c1(voice, 0, dacs, 0);
AudioConnection          c2(voice, 0, dacs, 1);

MicrocosmosFaustProgram::MicrocosmosFaustProgram() : Program() {
  Setup();
}

void MicrocosmosFaustProgram::Setup() {
  AudioMemory(40);
  AudioInterrupts();

  _page = 0;
  _lastNote = -1;
  _noteOffset = 0;

  for (int i = 0; i < MENU_PARAMS; i++) {
    _menuParams[i] = {
      menuParams[i].address,
      menuParams[i].page,
      menuParams[i].param,
      menuParams[i].display,
      menuParams[i].value,
      menuParams[i].controller,
      menuParams[i].locked
    };
    if (_menuParams[i].param != NULL) {
      voice.setParamValue(_menuParams[i].param, _menuParams[i].value);
    }
  }

  updateMenu();
}

void MicrocosmosFaustProgram::Run() {
}

void MicrocosmosFaustProgram::Loop() {
  Engine::Instance()->oled->SetScale(2);
  Engine::Instance()->oled->DrawParamLine(0, pageTitles[_page], _page);
  Engine::Instance()->oled->SetScale(1);

  int param = 0;
  for (int i=0; i<MENU_PARAMS; i++) {
    if (_menuParams[i].page == _page) {
      if (_menuParams[i].param != NULL) {
        Engine::Instance()->oled->DrawParamLine(param + 2, _menuParams[i].display, _menuParams[i].value);
      }
      param ++;
    }
  }
}

void MicrocosmosFaustProgram::OnKeyDown(int key) {
  int notes[4] = {48, 51, 55, 58};

  Serial.println(key);

  if (key < 4) {
    voice.setParamValue("gate", true);
    voice.setParamValue("note", notes[key]);
    Engine::Instance()->buttonLeds[key]->Set(5);
  } else if (key >= 4) {
    _page = (_page + 1) % MENU_PAGES;
    updateMenu();
  }
}

void MicrocosmosFaustProgram::OnKeyUp(int key) {
  if (key < 4) {
    voice.setParamValue("gate", false);
  }
}

void MicrocosmosFaustProgram::OnPotChange(int index, int value) {
  Serial.print(index);
  Serial.print(' ');
  Serial.println(value);

  if (index == 0) {
    // TODO: BUGGGGG!
    MenuParam *p = getPageParam(3);
    int mappedValue = value;

    if (p->param != NULL && mappedValue != p->value) {

      // unlock if the value is the same as in memory
      if (p->locked) {
        if (mappedValue == p->value) {
          p->locked = false;
        }
      }

      // update only if param is unlocked (and value is different from memory)
      if (!p->locked) {
        p->value = mappedValue;
        voice.setParamValue(p->param, p->value);
      }
    }
  } else if (index == 1) {
    _noteOffset = value;
    //voice.setParamValue("master_volume", Engine::Instance()->pots[2]->ReadAs64());
  } else if (index == 2) {
    voice.setParamValue("master_volume", value);
  }
}

void MicrocosmosFaustProgram::OnEncoderChange(int index, int value) {
  // constrain the value beetween 0 and 127
  value = value <= 0 ? 0 : value;
  value = value >= 127 ? 127 : value;
  Engine::Instance()->encoders[index]->Reset(value);

  MenuParam *p = getPageParam(index);

  if (p->param != NULL && value != p->value) {
    p->value = value;
    voice.setParamValue(p->param, p->value);
  }
}

void MicrocosmosFaustProgram::OnGateIn(uint8_t channel) {
  // NOT AVAILABLE ON THIS BOARD
}

void MicrocosmosFaustProgram::OnNoteOn(byte channel, byte note, byte velocity) {
  _lastNote = note;
  voice.setParamValue("gate", true);
  voice.setParamValue("note", note + _noteOffset);
}

void MicrocosmosFaustProgram::OnNoteOff(byte channel, byte note, byte velocity) {
  if (_lastNote == note) {
    voice.setParamValue("gate", false);
  }
}

void MicrocosmosFaustProgram::OnControlChange(byte channel, byte control, byte value) {
  bool changed = false;

  for (int i=0; i<MENU_PARAMS; i++) {
    if (_menuParams[i].controller == control) {
      if (_menuParams[i].param != NULL) {
        _menuParams[i].value = value;
        _page = _menuParams[i].page;
        voice.setParamValue(_menuParams[i].param, value);
        changed = true;
        break;
      }
    }
  }

  if (changed) {
    updateMenu();
  }
}

void MicrocosmosFaustProgram::updateMenu() {
  int param = 0;
  for (int i=0; i<MENU_PARAMS; i++) {
    if (_menuParams[i].param != NULL) {
      _menuParams[i].locked = true;
      if (_menuParams[i].page == _page) {
        if (param < 3) {
          Engine::Instance()->encoders[param]->Reset(_menuParams[i].value);
        }
        param ++;
      }
    }
  }
}

MenuParam* MicrocosmosFaustProgram::getPageParam(int num) {
  int param = 0;
  for (int i=0; i<MENU_PARAMS; i++) {
    if (_menuParams[i].page == _page) {
      if (param == num) {
        return &_menuParams[i];
      }
      param ++;
    }
  }
  return NULL;
}
