# Faselunare || Microcosmos

Microcosmos is a small (130X80mm) open-source electronic board, developed by Faselunare (Italy), aimed at prototyping electronic musical instruments and learning electronics, microcontroller programming and audio DSP.

Microcosmos is made on top of a Teensy 4.1 board and features several encoders, buttons, it has an OLED color display, SD Card, audio I/O, MIDI I/O on Mini Jack and MIDI over USBHost.



### faust compilation fix:

Before this program can be compiled and uploaded to the Teensy 3.6, some modifications need to be made to the configuration file used by the compilation script used by Teensyduino (boards.txt). You should be able to find it in hardware/teensy/avr in the source of the Arduino software (its location will vary depending on the platform your using). The most important thing to do here is to use g++ instead of gcc for linking, so:

```
teensy36.build.command.linker=arm-none-eabi-gcc
```

should become:

```
teensy36.build.command.linker=arm-none-eabi-g++
```

in boards.txt. Beware that on older versions of Teensyduino, these changes should be made directly to platform.txt.

After making these changes, you should be able to compile and upload your sketch to the Teensy 3.6.


## Credits

Board developed and assembled By Francesco Mulassano and Alessandro Comanzo.
Software developed by Daniele Pagliero.

## License

This project is licensed under the terms of the GNU GENERAL PUBLIC LICENSE.
